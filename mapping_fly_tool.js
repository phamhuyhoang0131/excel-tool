const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://hotelgateway:SeechoitlOfwoRwObEaphOabdLVy@54.169.228.218:3306/HotelGateway'
})
const csv = require('fast-csv')
const fs = require('fs')
const _ = require('lodash')
const json2csv = require('json2csv')
function readCsv(path) {
    return new Promise((resolve, reject) => {
        let csv_arr = []
        fs.createReadStream(path)
            .pipe(csv())
            .on("data", function (data) {
                csv_arr.push(data)
            })
            .on("end", function () {
                resolve(csv_arr)
            });
    })

}

async function progess() {
    let csv_data = await readCsv('./Nok-Air-Flight-destinations-to-be-mapped.csv')
    let promises = []
    let airports_mapping = []
    let list_city_not_mapping = []
    let list_city_mapping = []
    csv_data = csv_data.slice(22, csv_data.length)

    csv_data.map(data => {
        let [cityName, aiportCode] = data
        cityName = cityName.split('(').map(name => name.trim())[0]
        // search when city_not_mapping table
        promises.push(knex('city_not_mapping').whereRaw(`provider_city_name like '%${cityName}%' and (provider_country_code = ? or country_code = ?) `, ['TH', 'TH']))
    })
    csv_data.map(data => {
        let [cityName, aiportCode] = data
        cityName = cityName.split('(').map(name => name.trim())[0]
        // denied mapped airport code
        if (cityName != 'Sukhothai' && cityName != 'Phang Nga') {
            airports_mapping.push(knex('lst_airport').insert({
                code: aiportCode,
                city_name: cityName,
                country_code: 'TH',
                utc_timezone_offset: 7,
                timezone: 'Asia/Bangkok',
                note: 'Nok Air',
                del_flag: 1
            }).returning('id'))
        }
    })
    let result = await Promise.all(promises)
    result.map((data, idx) => {
        if (data.length) {
            let [cityName, aiportCode] = csv_data[idx]
            data.map(provider => {
                let { id, provider_code, provider_city_code, provider_city_name, provider_country_code, country_code } = provider
                // insert mapping data 
                list_city_mapping.push(knex('city_mapping').insert(
                    {
                        airport_code: aiportCode,
                        city_name: cityName,
                        provider_code: provider_code,
                        provider_city_code: provider_city_code,
                        provider_city_name: provider_city_name,
                        provider_country_code: provider_country_code || country_code,
                        note: 'Nok Air'
                    }).returning('id'))
            })
        } else {
            list_city_not_mapping.push(csv_data[idx])

        }
    })
    // let allInsert = await Promise.all(list_city_mapping)
    // insert airport missing from Nok Air
    let airport_result = await Promise.all(airports_mapping)
    console.log(airport_result)
    // console.log(allInsert)
    // console.log(list_city_not_mapping)
}
progess()




