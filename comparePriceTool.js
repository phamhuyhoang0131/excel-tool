const Promise = require('bluebird')
let request = Promise.promisifyAll(require('request'))
const _ = require('lodash')
const fs = require('fs')
const airports = ['KUL', 'HND','NRT', 'KIX', 'ICN']
const pages = [1, 2, 3, 4, 5, 6]
const supplier = ['FIT', 'EAN']
const stringCompare = require('./helper/string-similar')
const json2csv = require('json2csv')

const FITRequest = async() => {
    // make request for FIT
    let totalRequest = []

    airports.map(code => {
        let options = {
            method: 'POST',
            url: 'https://gateway.k9s.goquo.com/hotel/prod/get-availability',
            headers: {
                'postman-token': '1ad85141-b2b9-fe27-3cfb-7749b01b549e',
                'cache-control': 'no-cache',
                'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOiIyMDE4LTExLTIzVDA5OjU1OjM0LjU3OFoiLCJlbWFpbCI6ImxpdmVfZml0X2VhbkBnb3F1by5jb20ifQ.qSh_PINcDyPlSwYuEnl8Fdql62SAr0PkSGvpMO4xwTI',
                'x-key': 'live_fit_ean@goquo.com',
                'content-type': 'application/json'
            },
            body: {
                checkinDate: '10/04/2018',
                checkoutDate: '13/04/2018',
                roomList: [{
                    numberOfAdults: 2
                }],
                airportCode: code,
                isPackage: false,
                providerCode: 'FIT',
                numberOfResults: 100,
                languageCode: 'en-us',
                minStar: 0,
                pageNo: 1,
                timeout: 50000,
                customerIpAddress: '118.69.100.78',
                customerUserAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/601.6.17 (KHTML, like Gecko) Version/9.1.1 Safari/601.6.17',
                customerSessionId: 'yuvb3jdpifp2t13y43pass2p',
                passengerNationality: 'VN',
                passengerCountryOfResidence: 'VN'
            },
            json: true
        };
        totalRequest.push(options)
    })

    return totalRequest
}


const EANRequest = async() => {
    // make request for EAN

    let totalRequest = []

    airports.map(code => {
        pages.map(page => {

            let options = {
                method: 'POST',
                url: 'https://gateway.k9s.goquo.com/hotel/prod/get-availability',
                headers: {
                    'postman-token': '1ad85141-b2b9-fe27-3cfb-7749b01b549e',
                    'cache-control': 'no-cache',
                    'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOiIyMDE4LTExLTIzVDA5OjU0OjIzLjAzNFoiLCJlbWFpbCI6ImxpdmVAZ29xdW8uY29tIn0.kF6qToda3VIRxRc9NMqsB54AKM0e9gmIhZ9t_zlze_A',
                    'x-key': 'live@goquo.com',
                    'content-type': 'application/json'
                },
                body: {
                    checkinDate: '10/04/2018',
                    checkoutDate: '13/04/2018',
                    roomList: [{
                        numberOfAdults: 2
                    }],
                    airportCode: code,
                    isPackage: false,
                    providerCode: 'EAN',
                    numberOfResults: 100,
                    languageCode: 'en-us',
                    minStar: 0,
                    pageNo: page,
                    timeout: 50000,
                    customerIpAddress: '118.69.100.78',
                    customerUserAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/601.6.17 (KHTML, like Gecko) Version/9.1.1 Safari/601.6.17',
                    customerSessionId: 'yuvb3jdpifp2t13y43pass2p',
                    passengerNationality: 'VN',
                    passengerCountryOfResidence: 'VN'
                },
                json: true
            };

            totalRequest.push(options)
        })
    })
    return totalRequest
}



const handleResults = (requestArr) => {
    const toRequest = {}
    airports.map(code => {
        const rqMapping = Promise.all(requestArr.filter(r => r.body.airportCode == code).map(rq => {
            return request.postAsync(rq).then(RS => {
                return RS.body
            })
        }))
        toRequest[code] = rqMapping
    })
    return Promise.props(toRequest)

}




const getXpediaId = async(hotelIds, Name = 'FIT') => {
    let mappingObj = {
        Name,
        CityCode: "",
        ListIds: hotelIds
    }
    let options = {
        body: mappingObj,
        json: true,
        timeout: parseInt(process.env.HOTEL_MAPPING_TIMEOUT) || 2000, // ms
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'x-access-token': '9251B56E5FBE5BDC5F84ED4CDBF621'
        }
    }

    return request.postAsync('http://hotelmapping.goquo.com/api/hotelmapping/postsuppliersv3', options)
        .then(RS => {
            return RS.body
        })
}


const getHotelIds = async() => {


    const FITRS = await FITRequest().then(handleResults)
    fs.writeFileSync('FITTestRS.json', JSON.stringify(FITRS))
    const EANRS = await EANRequest().then(handleResults)
    fs.writeFileSync('EANTestRS.json', JSON.stringify(EANRS))


    const source = require('./FITTestRS.json');
    const EANSource = require('./EANTestRS.json')



    let EANHotels = []
    let hotels = []



    for (var key in source) {
        if (source.hasOwnProperty(key)) {
            var element = source[key];
            element.map(e => {
                hotels.push(e.FIT.hotelSummary)
            })

        }
    }

    for (var key in EANSource) {
        if (EANSource.hasOwnProperty(key)) {
            var element = EANSource[key];
            element.map(e => {
                EANHotels.push(e.EAN.hotelSummary)
            })
        }
    }
    // extract hotelIds
    EANHotels = _.compact(_.flattenDeep(EANHotels))
    let EANHotelIds = _.uniq(EANHotels.map(hotel => hotel.hotelId))
    console.log("Số lượng hotel của EAN : " + EANHotelIds.length)
    hotels = _.flattenDeep(hotels)
    let hotelIds = hotels.map(hotel => hotel.hotelId)
    hotelIds = _.uniq(hotelIds)
    console.log("Số lượng hotel của FIT : " + hotelIds.length)


    // send to mapping services to get expedia id
    const xpediaIds = await getXpediaId(hotelIds)
    console.log("Số lượng hotel FIT có mapping: " + Object.keys(xpediaIds).length)
    const EANXpediaIds = await getXpediaId(EANHotelIds, 'EAN')
    console.log("Số lượng hotel EAN có mapping : " + Object.keys(EANXpediaIds).length)
    // get origin hotel is mapped from FIT
    let finalFITHotelMapped = []
    for (var key in xpediaIds) {
        if (xpediaIds.hasOwnProperty(key)) {
            var element = xpediaIds[key];
            let matchHotel = (hotels.find(hotel => hotel.hotelId == key))
            matchHotel.expediaId = element
            finalFITHotelMapped.push(matchHotel)
        }
    }
    // get origin hotel is mapped from EAN
    let finalEANHotelMapped = []
    for (var key in EANXpediaIds) {
        if (EANXpediaIds.hasOwnProperty(key)) {
            var element = EANXpediaIds[key];
            let matchHotel = (EANHotels.find(hotel => hotel.hotelId == key))
            matchHotel.expediaId = element
            finalEANHotelMapped.push(matchHotel)
        }
    }

    const hotelsMapped = []
    // find hotel is same bettween FIT and EAN
    finalEANHotelMapped.map(EANHotel => {
        const FITHotel = finalFITHotelMapped.find(f => f.expediaId == EANHotel.expediaId)
        if (FITHotel) {
            hotelsMapped.push({
                EANHotelId: EANHotel.hotelId,
                FITHotelId: FITHotel.hotelId,
                expediaId: EANHotel.expediaId,
                rooms: {
                    EANRooms: EANHotel.roomList,
                    FITRooms: FITHotel.roomList
                },
                rates: compareRate(EANHotel.roomList, FITHotel.roomList, EANHotel.hotelId, FITHotel.hotelId)
            })
        }
    })
    console.log("Số lượng hotel Mapped giữa FIT và EAN => " + hotelsMapped.length)

    fs.writeFileSync('CompareRatesResult.json', JSON.stringify(hotelsMapped))

}

const getProp = (obj, name) => {
    var realName = _.findKey(obj, function (value, key) {
        return key.toLowerCase() === name.toLowerCase();
    });
    return obj[realName];
};

const transformData = async() => {
    const rawData = require('./CompareRatesResult.json')
    console.log("Tổng số hotel " + rawData.length)
    // filter hotel have no room same 
    const newData = rawData.filter(r => r.rates.length)
    console.log("Tổng số hotel có room trùng  " + newData.length)

    // restucture return data
    const csvData = []
    return newData.map(data => {
        const {
            EANHotelId,
            FITHotelId,
            rooms,
            rates
        } = data
        // progess data for rates 
        let convertRates = rates.map(rate => {
            const {
                eanRates,
                fitRates
            } = rate
            let eanRateType1 = getProp(eanRates, 'room only')
            let fitRateType1 = getProp(fitRates, 'room only')
            let eanRateType2 = getProp(eanRates, 'Breakfast')
            let fitRateType2 = getProp(fitRates, 'Breakfast')
            if (eanRateType1 && fitRateType1 && eanRateType2 && fitRateType2) {
                return [{
                    EANHotelId,
                    FITHotelId,
                    EANRate: eanRateType1[0].name,
                    EANPrice: eanRateType1[0].price,
                    EANRoomTypeCode: eanRates.roomTypeCode,
                    EANRoomName: eanRates.roomName,
                    FITRate: fitRateType1[0].name,
                    FITPrice: fitRateType1[0].price,
                    FITRoomName: fitRates.roomName,
                    FITRoomTypeCode: fitRates.roomTypeCode
                }, {
                    EANHotelId,
                    FITHotelId,
                    EANRate: eanRateType2[0].name,
                    EANPrice: eanRateType2[0].price,
                    EANRoomTypeCode: eanRates.roomTypeCode,
                    EANRoomName: eanRates.roomName,
                    FITRate: fitRateType2[0].name,
                    FITPrice: fitRateType2[0].price,
                    FITRoomName: fitRates.roomName,
                    FITRoomTypeCode: fitRates.roomTypeCode
                }]
            }
            if (eanRateType1 && fitRateType1) {
                return [{
                    EANHotelId,
                    FITHotelId,
                    EANRate: eanRateType1[0].name,
                    EANPrice: eanRateType1[0].price,
                    EANRoomTypeCode: eanRates.roomTypeCode,
                    EANRoomName: eanRates.roomName,
                    FITRate: fitRateType1[0].name,
                    FITPrice: fitRateType1[0].price,
                    FITRoomName: fitRates.roomName,
                    FITRoomTypeCode: fitRates.roomTypeCode
                }]
            }
            if (eanRateType2 && fitRateType2) {

                return [{
                    EANHotelId,
                    FITHotelId,
                    EANRate: eanRateType2[0].name,
                    EANPrice: eanRateType2[0].price,
                    EANRoomTypeCode: eanRates.roomTypeCode,
                    EANRoomName: eanRates.roomName,
                    FITRate: fitRateType2[0].name,
                    FITPrice: fitRateType2[0].price,
                    FITRoomName: fitRates.roomName,
                    FITRoomTypeCode: fitRates.roomTypeCode
                }]
            }

        })

        convertRates = _.flatten(convertRates)
        return convertRates
    })

}

const compareRate = (EANRooms, FITRooms, eanHotelId, fitHotelId) => {
    const rates = []
    EANRooms.map((roomRequest, idx) => {
        const fitRooms = FITRooms[idx].rooms
        if (fitRooms) {
            const listFitRoomsName = fitRooms.map(f => f.roomName)
            const listEanRoomsName = roomRequest.rooms.map(f => f.roomName)
            // compare room name 
            const similarRoomNames = stringCompare.map2Arr(listEanRoomsName, listFitRoomsName)
            similarRoomNames.map((s, idx) => {
                const eanRoom = roomRequest.rooms[idx];
                // check if matched 90% or higher
                if (s.rating > 0.9) {
                    const index = listFitRoomsName.indexOf(s.target)
                    const fitRoom = fitRooms[index]
                    // after verify room is same compare price 
                    const eanRates = _.groupBy(eanRoom.rateList, 'name')
                    const fitRates = _.groupBy(fitRoom.rateList, 'name')
                    // add roomName for result 
                    eanRates.roomName = eanRoom.roomName
                    eanRates.roomTypeCode = eanRoom.roomTypeCode
                    fitRates.roomName = fitRoom.roomName
                    fitRates.roomTypeCode = fitRoom.roomTypeCode
                    rates.push({
                        eanRates,
                        fitRates
                    })

                }
            })

        }
    })
    return rates

}


const getHotelInfo = async(hotelIds) => {
    const subs = _.chunk(hotelIds, 20)
    const allRequest = []
    subs.map(s => {
        allRequest.push(request.postAsync({
            method: 'POST',
            url: 'https://offer.k9s.goquo.com/prod/infosvc/hotelinfo',
            headers: {
                'postman-token': '8fbb23a3-73af-a9e4-664b-b7d10d3155ca',
                'cache-control': 'no-cache',
                authorization: 'Basic Z29xdW86MTIzMTIz',
                'content-type': 'application/json'
            },
            body: {
                source_id: 'ea4bcq4hbak',
                ids: s,
                language_code: 'en-US'
            },
            json: true
        }).then(RS => {
            return RS.body.data
        }))
    })
    return Promise.all(allRequest)
}


const finalStep = async(hotels) => {
    hotels = _.compact(hotels)
    const EANHotelIds = _.uniq(hotels.map(hotel => hotel.EANHotelId))
    let hotelInfos = await getHotelInfo(EANHotelIds)
    hotelInfos = _.flatten(hotelInfos)
    console.log(hotelInfos)
    // add hotel name 
    hotels = hotels.map(hotel => {
        hotel.hotelName = hotelInfos.find(h => h.hotel_id == hotel.EANHotelId).name
        return hotel
    })
    return hotels
}

getHotelIds().then(transformData).then(data => {
    return _.flatten(data)
}).then(finalStep).then(finalData => {
    fs.writeFileSync('RatesCompare.json', JSON.stringify(finalData))

})
