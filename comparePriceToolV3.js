const Promise = require('bluebird')
let request = Promise.promisifyAll(require('request'))
const _ = require('lodash')
const fs = require('fs')
const airports = ['KUL', 'HND', 'NRT', 'KIX', 'ICN']
const pages = [1, 2, 3, 4, 5, 6]
const supplier = ['FIT', 'EAN']
const stringCompare = require('./helper/string-similar')
const json2csv = require('json2csv')

const source_ids = ['fitmdwsnaqc', 'ea4bcq4hbak']


function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


const getHotelInfo = async (hotelIds) => {
    const subs = _.chunk(hotelIds, 20)
    const allRequest = []
    subs.map(s => {
        allRequest.push(request.postAsync({
            method: 'POST',
            url: 'https://offer.k9s.goquo.com/prod/infosvc/hotelinfo',
            headers: {
                'postman-token': '8fbb23a3-73af-a9e4-664b-b7d10d3155ca',
                'cache-control': 'no-cache',
                authorization: 'Basic Z29xdW86MTIzMTIz',
                'content-type': 'application/json'
            },
            body: {
                source_id: 'ea4bcq4hbak',
                ids: s,
                language_code: 'en-US'
            },
            json: true
        }).then(RS => {
            return RS.body.data
        }))
    })
    return Promise.all(allRequest)
}



const getHotelRQ = () => {
    const RS = airports.map(airport => {
        return {
            method: 'POST',
            url: 'https://offer.k9s.goquo.com/dev/svc/search',
            headers: {
                'postman-token': 'bfedefe9-6074-4057-c7a2-d444cd03e102',
                'cache-control': 'no-cache',
                'x-key': 'tuananh@goquo.com',
                'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTAwLCJlbWFpbCI6InR1YW5hbmhAZ29xdW8uY29tIiwiZXhwaXJlIjoxNTEyMzUyMzU1fQ.CBOBne1Fa8MVWVXhkTj-7NvfMIIWiYjYa-LUb8cGXvQ',
                'content-type': 'application/json'
            },
            body: {
                checkin: '2018-04-10',
                checkout: '2018-04-13',
                destination_code: airport,
                package: true,
                language_code: 'en-US',
                rooms: [{
                    idx: 1,
                    adults: 2
                }],
                ip: '127.0.0.2',
                user_agent: 'Mozilla',
                session_id: '123123',
                nationality: 'MY',
                country_residence: 'VN',
                wait_time: '50000' // default wait for 50 sec
            },
            json: true
        };
    })
    return Promise.all(RS.map(rs => request.postAsync(rs).then(RS => RS.body)))
}



const filterHotelBySourceId = async(responseArr) => {
    let allHotels = []
    responseArr.map(res => {
        res.data.map(hotel => {
            // add requestId to get room by hotel
            hotel.request_id = res.request_id
            allHotels.push(hotel)
        })
    })
    // filter by source id
    const [FITSourceId, EANsourceId] = source_ids
    allHotels = allHotels.filter(hotel => hotel.source_id == FITSourceId || hotel.source_id == EANsourceId)
    console.log("số lượng trước khi unique => " + allHotels.length)
    allHotels = _.unionBy(allHotels, 'hotel_id')
    console.log("số lượng hotel sau cùng => " + allHotels.length)
    let EANHotels = []
    let FITHotels = []
    // clear format 
    EANHotels = allHotels.filter(hotel => hotel.source_id == EANsourceId)
    FITHotels = allHotels.filter(hotel => hotel.source_id == FITSourceId)
    console.log("số lượng hotel của EAN => " + EANHotels.length)
    console.log("số lượng hotel cuả FIT => " + FITHotels.length)
    // getRooms(allHotels)
    return {
        EANHotels,
        FITHotels,
        allHotels
    }

}

const getRooms = async(hotels) => {
    let {
        EANHotels,
        FITHotels,
        allHotels
    } = hotels

    allHotels = _.groupBy(allHotels, 'request_id')

    const allRQ = []
    for (let key in allHotels) {
        if (allHotels.hasOwnProperty(key)) {
            let element = allHotels[key];
            element = _.chunk(element, 100)
            element.map(pieces => {
                let options = {
                    method: 'POST',
                    url: 'https://offer.k9s.goquo.com/dev/svc/getrooms',
                    headers: {
                        'postman-token': '31fa96da-22be-d374-3c38-8374b60bd81c',
                        'cache-control': 'no-cache',
                        'x-key': 'tuananh@goquo.com',
                        'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTAwLCJlbWFpbCI6InR1YW5hbmhAZ29xdW8uY29tIiwiZXhwaXJlIjoxNTEyMzUyMzU1fQ.CBOBne1Fa8MVWVXhkTj-7NvfMIIWiYjYa-LUb8cGXvQ',
                        'content-type': 'application/json'
                    },
                    body: {
                        request_id: key,
                        hotels: pieces.map(e => {
                            const {
                                hotel_id,
                                source_id,
                                request_id
                            } = e
                            return {
                                source_id,
                                hotel_id
                            }
                        })
                    },
                    json: true
                };
                allRQ.push(options)

            })


        }
    }
    return Promise.all(allRQ.map(rq => request.postAsync(rq).then(RS => RS.body)))
}

const progessResult = async(results) => {
    const mappedHotels = []
    results = _.flatten(results.map(r => r.data))
    // group hotel by goquo Id
    results = _.groupBy(results, 'goquo_id')
    for (var key in results) {
        if (results.hasOwnProperty(key)) {
            var element = results[key];
            if (element.length == 2) {
                mappedHotels.push(element)
            }
        }
    }
    let finalData = mappedHotels.map(hotels => {

        let FITHotel = hotels.find(hotel => hotel.source_id == source_ids[0])
        let EANHotel = hotels.find(hotel => hotel.source_id == source_ids[1])
        let uniqueHotel = {
            goquo_id: FITHotel.goquo_id,
            ean_id: EANHotel.hotel_id,
            fit_id: FITHotel.hotel_id,
            // rooms: {
            //     eanRooms: EANHotel.rooms,
            //     fitRooms: FITHotel.rooms
            // },
            compareRates: []
        }

        const rates = []
        const FITRooms = _.uniq(FITHotel.rooms.map(room => room.room_name))


        const EANRooms = _.uniq(EANHotel.rooms.map(room => room.room_name))

        // compare room name only pick room match 90% or higher 
        const similarRoomNames = stringCompare.map2Arr(FITRooms, EANRooms)
        similarRoomNames.map((s, idx) => {

            const FITRoomMatch = FITHotel.rooms.filter(r => r.room_name == s.source)

            // generate token to confirm room is same 
            const token = makeid()
            if (s.rating >= 0.9) {
                const EANRoomMatch = EANHotel.rooms.filter(r => r.room_name == s.target)
                // get all rates 
                EANRoomMatch.map(room => {
                    room.rates.map(rate => {
                        rate.label = 'EAN'
                        rate.room_type_id = room.room_type_id
                        rate.room_name = room.room_name
                        rate.idx = room.idx
                        rate.token = token
                        rates.push(rate)
                    })
                })
                FITRoomMatch.map(room => {
                    room.rates.map(rate => {
                        rate.label = 'FIT'
                        rate.room_type_id = room.room_type_id
                        rate.room_name = room.room_name
                        rate.idx = room.idx
                        rate.token = token
                        rates.push(rate)
                    })
                })
            }
        })
        uniqueHotel.compareRates = rates
        // beautifulData(rates)
        return uniqueHotel
    })
    //filter hotel not have any rate same 

    finalData = finalData.filter(d => d.compareRates.length)
    console.log("number of hotel have same name => " + finalData.length)
    finalData = finalData.map(f => {
        f.compareRates = beautifulData(f.compareRates).
        filter(rate => rate.length).
        map(r => {
            return r.map(t => {
                delete t.rate_detail
                // delete t.room_type_id
                return t
            })

        })
        return f
    })
    let toReturn = []

    // remove hotel have not have same rates

    // flatten data return 
    finalData.map(hotel => {
        hotel.compareRates.map(room => {
            const {
                goquo_id,
                ean_id,
                fit_id
            } = hotel
            const EANRate = room.find(r => r.label == 'EAN')
            const FITRate = room.find(r => r.label == 'FIT')
            if (EANRate && FITRate) {
                toReturn.push({
                    goquo_id,
                    ean_id,
                    fit_id,
                    hotel_name: '',
                    board_basis: EANRate.board_basis,
                    ean_room_name: EANRate.room_name,
                    ean_room_type_id: EANRate.room_type_id,
                    ean_price: EANRate.price,
                    fit_room_name: FITRate.room_name,
                    fit_room_type_id: FITRate.room_type_id,
                    fit_price: FITRate.price
                })
            }


        })
    })
    const _hotels = _.unionBy(toReturn, 'goquo_id')

    console.log("số lượng hotel compare được => "+ _hotels.length)
    // get hotel name from api
    const eanHotelIds = _hotels.map(h => h.ean_id)

    let eanHotelInfos = await getHotelInfo(eanHotelIds)
    eanHotelInfos  = _.flatten(eanHotelInfos)
    // add hotelInfo
    toReturn = toReturn.map(t =>{
        const hotelName = eanHotelInfos.find(info => info.hotel_id == t.ean_id).name
        t.hotel_name = hotelName
        return t
    })
    fs.writeFileSync('CompareRatesV3.json', JSON.stringify(toReturn))
}

const beautifulData = (compareRates) => {
    let finalRates = []
    compareRates = _.groupBy(compareRates, 'token')
    for (var key in compareRates) {
        if (compareRates.hasOwnProperty(key)) {
            var element = compareRates[key];
            element = _.groupBy(element, 'board_basis')
            _.forOwn(element, (value, key) => {
                value = value.map(v => {
                    // remove unnessesary fildes
                    delete v.cancellation_text
                    delete v.cancellations
                    delete v.non_refundable
                    delete v.value_adds
                    delete v.promos
                    return v
                })
                let arr = _.uniqBy(value, 'rate_detail')
                finalRates.push(arr.length > 1 ? arr : []);
            })
        }
    }
    // cleanup result
    return finalRates
}

getHotelRQ().then(filterHotelBySourceId).then(getRooms).then(progessResult)