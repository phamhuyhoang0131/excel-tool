const csv = require('fast-csv')
const fs = require('fs')
const _ = require('lodash')
const json2csv = require('json2csv')


function readCsv(path) {
    return new Promise((resolve, reject) => {
        let csv_arr = []
        fs.createReadStream(path)
            .pipe(csv())
            .on("data", function (data) {
                csv_arr.push(data)
            })
            .on("end", function () {
                resolve(csv_arr)
            });
    })

}



const mappingISOCode = async() => {
    let data = await readCsv('./ListCountry_ISO_PrefixCode.csv')
    const header = data.shift()
    data = data.map(d => {
        const [countryName, countryCode, combineCode] = d
        const [ISOCode, ISOCodeThreeDot] = combineCode.split('/')
        return {
            countryName,
            phonePrefix: "+" + countryCode,
            ISOCode
        }
    })
    fs.writeFileSync('PrefixPhoneCode.json', JSON.stringify(data))
}


// mappingISOCode()



const convertLanguageList = async () =>{
    let data  = await readCsv('./language_codes.csv')
    const header = data.shift()
    data = data.map(d =>{
        const [name, code] = d
        return {
            name,
            code
        }
    })
    fs.writeFileSync('LanguageCodes.json', JSON.stringify(data))

}

convertLanguageList()


// const getDepartureTime  = () =>{
//     const str = "</strong></p>\n\nShiba Park Hotel at 07:20 am\n\nKeisei Ueno staion at 7:20am\n\nShiodome station at 7:30am\n\nMcDonald Suido-bashi station at 7:30am\n\nImperial Hotel at 7:40am\n\nHotel New Otani at 8am\n\nShinjuku Prince Hotel at 8am\n\n,<p>Times are subject to change due to local traffic conditions.</p>"
//     const pattern = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$/

//     var index = pattern.exec(str)
//     console.log(index)

// }

// getDepartureTime()



const str = 'RegExr v3 was 8:30-9:40 created by gskinner.com, and is proudly hosted by Media Temple.'
const pattern = /([A-Z])\w+/g

const timePattern = /\d{1,2}\s{0,2}(am|pm|([-|:|h| ]\s{0,2}\d{1,2}\s{0,2})?)\s{0,2}(-|to|till|,|untill)\s{0,2}\d{1,2}\s{0,2}(am|pm|([-|:|h| ]\s{0,2}\d{1,2}\s{0,2})?)/
console.log(str.match(timePattern)
)