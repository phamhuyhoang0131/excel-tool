const puppeteer = require('puppeteer');


(async () => {
  const browser = await puppeteer.launch({headless:false});
  const page = await browser.newPage();
  await page.goto('https://malindo-v3.goquo.vn/', {waitUntil: 'networkidle'});

  // click to search hotel
  await page.click('body > div.global-wrap > div.top-area.show-onload > div > div > div > div > div > div > ul > li:nth-child(3) > a > i', {clickCount:1})
  await page.waitFor(1000)

  // await page.screenshot({path: 'example.png'});
  await page.evaluate(() => {
    document.getElementById('fHotelSearch_DepartureDate').value = '13/10/2017'
    document.getElementById('fHotelSearch_ReturnDate').value = '14/10/2017'
  })
  // await page.waitFor(1000)
  // const searchBtnSelector = 'input[type=submit]'
  await page.click('#searchForm', {clickCount: 4})



})();
