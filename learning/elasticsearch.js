const el = require('elasticsearch')


var client = new el.Client({
  host: 'localhost:9200',
  log: 'trace'
});



client.ping({
  // ping usually has a 3000ms timeout 
  requestTimeout: 1000
}, function (error) {
  if (error) {
    console.trace('elasticsearch cluster is down!');
  } else {
    console.log('All is well');
  }
});




const createIndex = async (index) => {
    client.indices.create({
        index: index
    }, (error, res, status) => {
       return res
    })
}

const indexData = async (index, body, type) => {
    client.index({
        index,
        type,
        body
    }, (err, res) => {
        if(!err && res) return "success index"
        throw err
    })
}


const deleteIndex = async (index) => {
    client.indices.delete({
        index: index
    }, (err, res, status) =>{
        if(!err && res) return "success delete index"
        throw err
    })
}

module.exports = {
    createIndex,
    deleteIndex,
    indexData
}

deleteIndex('stories')