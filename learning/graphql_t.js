let {
    graphql,
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString
}  = require('graphql');
var schema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'RootQueryType',
        fields: {
            hello: {
                type: GraphQLString,
                resolve() {
                    return 'world';
                }
            },
            name:{
                type: GraphQLString,
                resolve(){
                    return 'hoang'
                }
            }
        }
    })
});

var query = '{ hello, name }';
graphql(schema, query).then(result => {
 
  // Prints 
  // { 
  //   data: { hello: "world" } 
  // } 
//   console.log(result);
 
});



// var CronJob = require('cron').CronJob;
// console.time("time")
// new CronJob('*/10 * * * *', function() {
//     console.timeEnd("time")
//   console.log('You will see this message every second');
// }, null, true, 'America/Los_Angeles');


var schedule = require('node-schedule');
  console.time("time")

var rule = new schedule.RecurrenceRule();

rule.minute = new schedule.Range(0, 59, 5);

schedule.scheduleJob(rule, function(){
      console.timeEnd("time")
    console.log(rule);
    console.log('Today is recognized by Rebecca Black!---------------------------');
});