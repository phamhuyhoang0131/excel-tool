const request = require('request-promise')

const Promise = require('bluebird')
const fs = require('fs')

const transform = require('camaro')
const builder = require('xmlbuilder')

const hotelIds = async() => {
    const rq = '{"Usercd":"SZ28276","Authno":"123456" ,"QueryType":"gainhotelids", "countrytype": "0"}'
    const options = {
        method: 'POST',
        gzip: true,
        uri: 'http://58.250.56.211:8081/common/service.do',
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept-Encoding': 'gzip',
            'Content-Encoding': 'gzip'
        },
        body: rq
    }
    const result = request(options).then(c => console.log(c))
}


// get hotel info


const hotelInfo = async() => {
    const rq = '{ "Usercd": "SZ28276", "Authno": "123456", "QueryType": "hotelinfo", "hotelIds": "1/2/3/4/5/6/7/76/86/87" }'

    const options = {
        method: 'POST',
        gzip: true,
        uri: 'http://58.250.56.211:8081/common/service.do',
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept-Encoding': 'gzip',
            'Content-Encoding': 'gzip'
        },
        body: rq
    }

    const result = request(options).then(c => fs.writeFileSync('HotelInfo.json', JSON.stringify(JSON.parse(c))))
}

// hotelInfo()

const Avail = async() => {
    const rq = '{"Usercd": "SZ28276","Authno": "123456","QueryType": "hotelpriceall","checkInDate": "2017-12-07","checkOutDate": "2017-12-09","hotelIds": "1/2/3/4/5/6/76","roomNum": "1","pricingtype": "12","roomtypeids": ""}'
    const options = {
        method: 'POST',
        gzip: true,
        uri: 'http://58.250.56.211:8081/common/service.do',
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept-Encoding': 'gzip',
            'Content-Encoding': 'gzip'
        },
        body: rq
    }
    await request(options).then(console.log)
}

// Avail().then()

const preBook = async() => {
    const rq = '{"Usercd": "SZ28276","Authno": "123456","QueryType": "checkprice","checkInDate": "2017-12-07","checkOutDate": "2017-12-09","roomNum": "2","pricingtype": "12","roomTypeId": "133876","rateType": "1"}'
    const options = {
        method: 'POST',
        gzip: true,
        uri: 'http://58.250.56.211:8081/common/service.do',
        headers: {
            'Content-Type': 'text/html;charset=UTF-8',
            'Accept-Encoding': 'gzip',
            'Content-Encoding': 'gzip'
        },
        body: rq
    }
    await request(options).then(console.log)
}

const booking = async () => {
    const buildOrderItems = (arrOrder) => {
        return arrOrder.map()
    }
    let requestObj = {
        Order: {
            Customercd: 'SZ28276',
            Authno: '123456',
            Businesstype: 'Neworder',
            Hotelid: 1,
            Roomtypeid: '133876',
            Isconfirm: 1,
            Pricingtype: 12,
            Checkindate: '2017-12-07',
            Checkoutdate: '2017-12-09',
            Roomqty: 2,
            Totalamount: 480,
            Checkinpersons: "huyhoang",
            Hotelremark: "Goodteaaspossible",
            Latestarrivaltime: "20:00",
            Arrivaltraffic: "Flight",
            Flight: 4009,
            Customerordercd: "Goquow1109200555",
            Contact: "DucThang",
            Contacttitle: "DucThang",
            Contactmobile: "13852525252",
            Contacttele: "075533397777",
            Contactfax: "075533397777",
            Contactemail: "phamhuyhoang995@gmail.com",
            Specialrequest: "Floorashighaspossible",
            Orderitems: [{
                Night: '2017-12-07 00:00:00',
                Ratetype: 1,
                Includebreakfastqty2: 7,
                Preeprice: 120
            }, {
                Night: '2017-12-08 00:00:00',
                Ratetype: 1,
                Includebreakfastqty2: 7,
                Preeprice: 120
            }]
            // breakfastItem 
        }
    }
    requestObj = builder.create(requestObj, {
        encoding: 'utf-8'
    }).end();

}

booking()