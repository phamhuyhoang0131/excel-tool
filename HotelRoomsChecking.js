const request = require('request-promise')

const Promise = require('bluebird')
const fs = require('fs')

const transform = require('camaro')
const builder = require('xmlbuilder')

const cities = ['BKK', 'HAN', 'NRT', 'LON']

const _ = require('lodash')


const getHotelInfo = async(hotelIds) => {
    const subs = _.chunk(hotelIds, 20)
    const allRequest = []
    subs.map(s => {
        allRequest.push(request({
            method: 'POST',
            url: 'https://offer.k9s.goquo.com/prod/infosvc/hotelinfo',
            headers: {
                'postman-token': '8fbb23a3-73af-a9e4-664b-b7d10d3155ca',
                'cache-control': 'no-cache',
                authorization: 'Basic Z29xdW86MTIzMTIz',
                'content-type': 'application/json'
            },
            body: {
                source_id: 'apyacwgpawq',
                ids: s,
                language_code: 'en-US'
            },
            json: true
        }).then(RS => {
            return RS.data
        }))
    })
    return Promise.all(allRequest)
}

var options = {
    method: 'POST',
    url: 'http://localhost:3000/get-availability',
    headers: {
        'postman-token': 'fed992dd-067e-67c7-719e-161bf6839851',
        'cache-control': 'no-cache',
        'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOiIyMDE4LTEyLTEyVDA5OjMzOjQ0LjExOFoiLCJlbWFpbCI6InR1YW5hbmhAZ29xdW8uY29tIn0.5tuRg-s0WaeZpZKDXkNfkoeD5PZ7jcB6yyffeg8AwlA',
        'x-key': 'tuananh@goquo.com',
        'content-type': 'application/json'
    },
    body: {
        checkinDate: '24/12/2017',
        checkoutDate: '25/12/2017',
        roomList: [{
            numberOfAdults: 2
        }],
        airportCode: 'BKK',
        isPackage: true,
        providerCode: 'APItude',
        numberOfResults: 100,
        languageCode: 'en-us',
        minStar: 0,
        pageNo: 60,
        timeout: 50000,
        customerIpAddress: '118.69.100.78',
        customerUserAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/601.6.17 (KHTML, like Gecko) Version/9.1.1 Safari/601.6.17',
        customerSessionId: 'yuvb3jdpifp2t13y43pass2p',
        passengerNationality: 'VN',
        passengerCountryOfResidence: 'VN'
    },
    json: true
};

const compareRates = async() => {
    const body = await request(options)
    try {
        let hotels = body.APItude.hotelSummary
        hotels = hotels.map(h => {
            let hotelInfo = {
                hotelId: h.hotelId,
                hotelName: ''
            }
            const {
                currencyCode,
                hotelId,
                provider,
                roomList
            } = h
            // compare rate for each room
            const rooms = roomList[0].rooms.map(room => {
                let rateList = room.rateList
                rateList = rateList.find(r => r.packaging == 'true') ? rateList : []

                // check if room have more than 2 rates 
                if (rateList.length > 2) {
                    // console.log('hotelid => ' + hotelInfo.hotelId)
                    const packagingRoom = rateList.filter(r => r.packaging == 'true')

                    const listRateName = _.uniq((packagingRoom.map(p => p.name)))
                    const rateSameName = rateList.filter(r => listRateName.includes(r.name))
                    room.rates = rateSameName
                    // delete list rate
                    delete room.rateList
                    return room
                }
            })
            hotelInfo.rooms = rooms
            return hotelInfo
        })
        // filter hotel have no packaging room
        hotels = hotels.map(h => {
            h.rooms = _.compact(h.rooms)
            return h
        }).filter(r => r.rooms.length > 0)

        // flatten data 
        const allRecords = []
        hotels = hotels.map(h => {
            let finalRooms = []

            h.rooms = h.rooms.map(r => {
                const {
                    roomTypeCode,
                    roomName,
                    rates
                } = r
                const roomInfo = {
                    roomTypeCode,
                    roomName
                }
                let packagingRates = rates.filter(rate => rate.packaging == 'true')
                const nomalRates = rates.filter(rate => rate.packaging == 'false')

                const minRoomOnlyRate = _.minBy(rates.filter(r => r.name == 'Room only'), 'price')
                const minBreakfastRate = _.minBy(rates.filter(r => r.name == 'Breakfast'), 'price')
                const minNomalRoomOnlyRate = _.minBy(nomalRates.filter(r => r.name == 'Room only'), 'price')
                const minNomalBreakfastRate = _.minBy(nomalRates.filter(r => r.name == 'Breakfast'), 'price')
                if (minRoomOnlyRate) {
                    roomInfo.roomOnlyPackagingPrice = minRoomOnlyRate.price
                }
                if (minBreakfastRate) {
                    roomInfo.BreakfastPackagingPrice = minBreakfastRate.price
                }
                if (minNomalRoomOnlyRate) {
                    roomInfo.minNomalRoomOnlyPrice = minNomalRoomOnlyRate.price
                }
                if (minNomalBreakfastRate) {
                    roomInfo.minNomalBreakfastPrice = minNomalBreakfastRate.price
                }

                if (minRoomOnlyRate && minBreakfastRate) {
                    roomInfo.roomOnlyPriceDifferencePercent = (minRoomOnlyRate.price / minBreakfastRate.price) * 100
                }
                if (minNomalRoomOnlyRate && minNomalBreakfastRate) {
                    roomInfo.roomBreakfastDifferencePercent = (minNomalRoomOnlyRate.price / minNomalBreakfastRate.price) * 100
                }

                // packagingRates.map((p, idx) => {
                //     const rateName = p.name
                //     const matchNomalRates = nomalRates.filter(n => n.name == rateName)

                //     const averageNomalRates = _.reduce(matchNomalRates, (sum, current) => {
                //         return sum += current.price
                //     }, 0) / (matchNomalRates.length)
                //     roomInfo[`packagingRate${idx}Name`] = rateName
                //     roomInfo[`packagingRate${idx}Price`] = p.price
                //     roomInfo[`averageNomalRates${idx}`] = averageNomalRates
                //     roomInfo[`packagingRateCode${idx}`] = p.rateCode
                //     roomInfo[`packagingRatePercent${idx}`] = ((p.price/ averageNomalRates)* 100)
                // })
                return roomInfo
            })
            return h

        })
        // flatten hotel data
        let hotelResults = []
        hotels = hotels.map(h => {
            h.rooms.map(r => {
                const data = {
                    hotelId: h.hotelId,
                    hotelName: h.hotelName,
                    roomTypeCode: r.roomTypeCode,
                    roomName: r.roomName,
                    roomOnlyPackagingPrice: r.roomOnlyPackagingPrice,
                    BreakfastPackagingPrice: r.BreakfastPackagingPrice,
                    minNomalRoomOnlyPrice: r.minNomalRoomOnlyPrice,
                    minNomalBreakfastPrice: r.minNomalBreakfastPrice,
                    roomOnlyPriceDifferencePercent: r.roomOnlyPriceDifferencePercent,
                    roomBreakfastDifferencePercent: r.roomBreakfastDifferencePercent
                }
                if (r.roomOnlyPackagingPrice && r.minNomalRoomOnlyPrice) {
                    hotelResults.push({
                        hotelId: h.hotelId,
                        hotelName: h.hotelName,
                        roomTypeCode: r.roomTypeCode,
                        roomName: r.roomName,
                        rateName: 'Room Only',
                        packagingRate: r.roomOnlyPackagingPrice,
                        nomalRate: r.minNomalRoomOnlyPrice,
                        percent: ((r.roomOnlyPackagingPrice/ r.minNomalRoomOnlyPrice ) * 100)
                    })
                }
                if (r.BreakfastPackagingPrice && r.minNomalBreakfastPrice) {
                    hotelResults.push({
                        hotelId: h.hotelId,
                        hotelName: h.hotelName,
                        roomTypeCode: r.roomTypeCode,
                        roomName: r.roomName,
                        rateName: 'Breakfast',
                        packagingRate: r.BreakfastPackagingPrice,
                        nomalRate: r.minNomalBreakfastPrice,
                        percent: (r.BreakfastPackagingPrice/ r.minNomalBreakfastPrice) * 100,
                    })
                }
            })
        })

        // get hotel name 
        let hotelsInfo = await getHotelInfo(hotelResults.map(h => h.hotelId))
        hotelsInfo = _.flatten(hotelsInfo)
        // console.log(hotelsInfo)
        // // add hotel name
        hotelResults = hotelResults.map(h =>{
            h.hotelName = hotelsInfo.find(info => info.hotel_id == h.hotelId).name
            return h
        })

        fs.writeFileSync('hotelCompareRate.json', JSON.stringify(hotelResults))
    } catch (error) {
        console.log(error)
    }
}

compareRates()