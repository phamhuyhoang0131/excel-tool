const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://hotelgateway:SeechoitlOfwoRwObEaphOabdLVy@54.169.228.218:3306/HotelGateway'
})
const csv = require('fast-csv')
const fs = require('fs')
const _ = require('lodash')
const json2csv = require('json2csv')
function readCsv(path) {
   return new Promise( (resolve, reject) => {
        let csv_arr = []
        fs.createReadStream(path)
            .pipe(csv())
            .on("data", function (data) {
                csv_arr.push(data)
            })
            .on("end", function () {
                resolve(csv_arr)
            });
    })

}
async function progess() {
    let csv_data = await readCsv('./gateway_city_mapping.csv')
    let agoda_hotels = await readCsv('./agoda_hotels_excityid.csv')
    // remove header file 
    csv_data = csv_data.slice(1, csv_data.length)
    let fields = agoda_hotels.slice(0, 1)[0]
    let hotel_not_mapping = []
    // add more fields 
    fields.push('city_code')
    agoda_hotels = agoda_hotels.slice(1, agoda_hotels.length)
    csv_data = csv_data.map(obj => {
        return {
            airport_code: obj[1],
            excityId: obj[3]
        }
    })
    let airport_ids = csv_data.map(obj => obj.airport_code)
    let sql_data = await knex.select('airport_code', 'provider_city_code').
    from('city_mapping').
    where('provider_code', 'Agoda').
    whereIn('airport_code', airport_ids).then(data => {
        return data.map(obj => {
            let raw = csv_data.find(s => s.airport_code === obj.airport_code)
            obj.excityId = raw.excityId
            return obj
        })
    })
    let combineData = agoda_hotels.map(obj => {

        let [agoda_hotelid, agoda_hotelname, excityid, exCityName] = obj
        let city_mapping = sql_data.find(e => e.excityId == excityid)
        if (city_mapping) {
            obj.push(city_mapping.provider_city_code)
        } else {
            // add to hotel not mapping yet
            hotel_not_mapping.push(obj)
            obj.push('')
        }
        return obj
    })
    return await {
        fields : fields,
        data: arr2Obj(combineData, fields)
    } 
}
function arr2Obj (arr, fields){
    return arr.map(obj => {
        let convertObj = {}
        fields.map((field,idx) => {
            convertObj[field] = obj[idx]
        })
        return convertObj
    })
}

async function mapByCityName(csv_path){
    let agoda_hotels = await readCsv(csv_path)
    // get header 
    let headers = agoda_hotels.slice(0,1)[0]
    // remove header from data 
    agoda_hotels = agoda_hotels.slice(1, agoda_hotels.length)
    let cities = agoda_hotels.map(hotel => hotel[3])
    // remove duplicate city name
    cities = _.uniq(cities)
    console.log(cities.length)
    cities = cities.join('|')
    // get city data from sql base on city name
    let sqlData = await knex.select('city_code', 'city_name').
                             from('provider_city').
                             whereRaw(`city_name REGEXP ?`,[cities]).
                             where('provider_code', 'Agoda')
    console.log(sqlData.length)
}
// progess().then(data => {
//     console.log(data.data.filter(s => s.city_code).length)
//     console.log(data.data.filter(s=> !s.city_code).length)
//     console.log(data.data.length)
//     let csv_data = json2csv(data)
//     fs.writeFile(`${__dirname}/agoda_hotels.csv`, csv_data, function (err) {
//         if (err) throw err;
//         console.log('file saved');
//     });
// })


mapByCityName('./agoda_hotels_excityid.csv')
