const transform  = require('camaro')
const promise = require('bluebird')

const request = promise.promisifyAll(require('request'))
const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://hotelgateway:SeechoitlOfwoRwObEaphOabdLVy@54.169.228.218:3306/HotelGateway'
})
const randomstring  = require('randomstring')
const fs = require('fs')

async function getMapping(){
        // list city airport mapping 
    let nokairResult = []
    const airports = await knex('city_mapping').select('airport_code', 'provider_code').where('note', 'Nok Air')
    airports.map(airport => nokairResult.push(makeRequest(airport.provider_code, airport.airport_code)))

    // get all result 
    const apiResults = await promise.all(nokairResult)
    // logs
    fs.writeFileSync('nokairMappingHotelResult.json', JSON.stringify(apiResults))
    console.log("done write file ")
}

async function makeRequest(provider_code, airport_code) {
    var options = {
        method: 'POST',
        uri: 'http://api.hotel.goquo.com/get-availability',
        headers:
        {
            'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOiIyMDE4LTA0LTIxVDA3OjIyOjI1Ljk0NFoiLCJlbWFpbCI6ImxpdmVAZ29xdW8uY29tIn0.xZT1jiDc1L4gucOklOVhpCp4zrKZa6tmclHaNA53wvI',
            'x-key': 'live@goquo.com',
            'content-type': 'application/json'
        },
        body:
        {
            checkinDate: '24/12/2017',
            checkoutDate: '25/12/2017',
            roomList: [{ numberOfAdults: 2 }],
            airportCode: airport_code,
            isPackage: false,
            providerCode: provider_code,
            numberOfResults: 89,
            languageCode: 'en-us',
            minStar: 0,
            pageNo: 1,
            timeout: 48000,
            customerIpAddress: '118.69.100.78',
            customerUserAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/601.6.17 (KHTML, like Gecko) Version/9.1.1 Safari/601.6.17',
            customerSessionId: randomstring.generate(24),
            passengerNationality: 'VN',
            passengerCountryOfResidence: 'VN'
        },
        json: true
    };
    return request.postAsync(options).then(res => {
        res.body.provider_code = provider_code
        res.body.airport = airport_code
        return res.body
    })
}


const listAirportMapped = ['AM1', 'KB1'
    , 'CK1', 'CH1', 'KN1', 'KL1', 'KS1', 'KW1', 'MH1', 'PG1'
    , 'PC1', 'PK1', 'RP1', 'SW1', 'ST1', 'SR1', 'TA1', 'TN1',
    'TS1', 'UD1', 'YS1', 'KD0', 'LT0', 'LP0', 'MK0', 'NY0',
    'NG0', 'TSP', 'PP0', 'USM', 'TA0'
]



function filterResult(airportMapped){
    let json  = require('./nokairMappingHotelResult.json')
    let finalResult = listAirportMapped.map(airport => {
        // console.log('list result by airport : '+ airport)
        let numberOfHotels = 0
        json.filter( j => j.airport == airport).map(data => {
            let provider = data[data.provider_code]
            numberOfHotels +=  provider.hotelSummary ?  provider.hotelSummary.length :  0 
        })
        return {
            airport,
            numberOfHotels
        }
    })
    fs.writeFileSync('finalResult.json', JSON.stringify(finalResult))
}



// const total = [1,2,4,5,6,7].reduce((total, currentValue) =>{ 
//     return total +currentValue
// }, 0)
// console.log(total)


// getMapping().then()

// filterResult()