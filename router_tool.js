let excel = require('exceljs')
let csv = require('fast-csv')
let fs = require('fs')
let _ = require('lodash')


let departure = ['TPE', 'KHH', 'HKG']



let lv1 = [{ idx: 0, name: 'NRT' }, { idx: 0, name: 'KIX' }, { idx: 0, name: 'OKA' }, { idx: 1, name: 'NRT' }, { idx: 2, name: 'NRT' }]

let lv2 = [
    { idx: 0, name: 'TPE' },
    { idx: 0, name: 'KHH' }, { idx: 0, name: 'CTS' },
    { idx: 0, name: 'HKD' }, { idx: 0, name: 'OKA' },
    { idx: 0, name: 'KIX' }, { idx: 0, name: 'ASJ' },
    { idx: 1, name: 'TPE' }, { idx: 1, name: 'NRT' }, { idx: 1, name: 'ASJ' },
    { idx: 2, name: 'TPE' }, { idx: 2, name: 'NRT' }
]

let lv3 = [
    { idx: 0, name: '' }, { idx: 1, name: '' }, { idx: 2, name: 'NRT' },
    { idx: 3, name: 'NRT' }, { idx: 4, name: 'TPE' }, { idx: 4, name: 'NRT' },
    { idx: 5, name: 'TPE' }, { idx: 5, name: 'NRT' }, { idx: 6, name: 'NRT' },
    { idx: 6, name: 'KIX' }
]

let lv4 = [
    { idx: 0, name: '' }, { idx: 1, name: '' }, { idx: 2, name: 'TPE' },
    { idx: 2, name: 'KHH' }, { idx: 3, name: 'TPE' }, { idx: 3, name: 'KHH' },
    { idx: 4, name: '' }, { idx: 5, name: 'TPE' }, { idx: 5, name: 'KHH' },
    { idx: 6, name: '' }, { idx: 7, name: 'TPE' }, { idx: 7, name: 'KHH' },
    { idx: 8, name: 'TPE' }, { idx: 8, name: 'KHH' }, { idx: 9, name: 'TPE' }
]

let allLevel = {
    lv1: lv1,
    lv2: lv2,
    lv3: lv3,
    lv4: lv4
}

function findMyWay(currentPosition, destinationPosition){
    let root = departure.findIndex( d => d === currentPosition),
        allRouter = []
    // search lv 1
     lv1.map(obj => {
        if ((obj.idx === root) && (obj.name === destinationPosition)){
            allRouter.push(`${currentPosition} - ${obj.name}`)
        }
        return obj
    })
    // search lv 2 
     lv2.map(obj => {
         if (obj.name === destinationPosition) {
             try {
                 // find parent
                 let parent = lv1[obj.idx].name
                 allRouter.push(`${currentPosition} - ${parent} - ${obj.name}`)
             } catch (error) {
                console.log("search lv 2 not found ")
             }

         }
     })
    // search lv 3
    lv3.map(obj => {
        if (obj.name === destinationPosition) {
            try {
                // find parent
                let parent_2 = lv2[obj.idx],
                    parent_1 = lv1[parent_2.idx]
                allRouter.push(`${currentPosition} - ${parent_1.name} - ${parent_2.name} - ${obj.name}`)
            } catch (error) {
                console.log("search lv 3 not found ")
            }

        }
    })
    lv4.map(obj => {
        if(obj.name === destinationPosition){
            // find parent
            let parent_3 = lv3[obj.idx],
                parent_2 = lv2[parent_3.idx],
                parent_1 = lv1[parent_2.idx]
            // return router 
            allRouter.push(`${currentPosition} - ${parent_1.name} - ${parent_2.name} - ${parent_3.name} - ${obj.name}` )
        }
    })
    console.log(allRouter)
}


class dataObj {
    constructor(name, idx) {
        this.name = name
        this.idx = idx
    }
}
function combineArray(parent_arr, child_arr, realIdx){
    let combineData = [],
        rootIndex = 0,
        idxArr =[]
    realIdx = realIdx ? realIdx : []
    parent_arr.map((obj, idx)=>{
        rootIndex = obj ? idx : rootIndex
        // get all rootIndex
        idxArr.push(rootIndex)
        combineData.push(new dataObj(child_arr[idx], rootIndex))
    })
    // filter rootIndex
    idxArr = _.uniq(idxArr)
    // filter empty 
    combineData.map((child) => {
        idxArr.map((value, idx) => {
            child.idx = (child.idx === value) ? (realIdx[idx] | idx) : child.idx
        })
        return child
    })

    return combineData.filter(c => c.name)
}

// findMyWay('TPE', 'HKD')

function readCsv(path) {
   return new Promise( (resolve, reject) => {
        let csv_arr = []
        fs.createReadStream(path)
            .pipe(csv())
            .on("data", function (data) {
                csv_arr.push(data)
            })
            .on("end", function () {
                resolve(csv_arr)
            });
    })

}
function splitByParent(parentArr, childArr) {

    let slice_arr = []
    for (var i = 0; i < parentArr.length; i++) {
        let start = parentArr[i].idx,
            end = parentArr[i + 1] ? parentArr[i + 1].idx : childArr.length
        // console.log( "bat dau :"+start)
        // console.log("end:" +end)
        slice_arr.push(childArr.slice(start, end))

    }
    return slice_arr
}

readCsv("./GOQUO-sales-routes-1.csv").then(data => {
    // remove two line header 
    data = data.slice(2, data.length)
    let department = [],
        allLevel = {
            lv1: [],
            lv2: [],
            lv3: [],
            lv4: []
        },
        allLevelResult = {
            lv1: [],
            lv2: [],
            lv3: [],
            lv4: []
        }

    data.map((obj, index) => {
        console.log(obj)
        if (obj[1]) {
            // push to department
            department.push(
                new dataObj(obj[1], index)
            )
        }
        // get data lv 1
        allLevel.lv1.push(obj[3])
        // get data lv2
        allLevel.lv2.push(obj[5])
        // get data lv 3
        allLevel.lv3.push(obj[7])
        // get data lv4
        allLevel.lv4.push(obj[9])
    })
   let lv1MapParent = splitByParent(department, allLevel.lv1)
   let lv2MapParent = splitByParent(department, allLevel.lv2)
   let lv3MapParent = splitByParent(department, allLevel.lv3)
   let lv4MapParent = splitByParent(department, allLevel.lv4)
   department.map((obj, idx) => {
       // progess data for lv 1
       lv1MapParent[idx].filter(obj => obj).map(lv1 => {
           allLevelResult.lv1.push(
               new dataObj(lv1, idx)
           )
       })
       // progess data for lv 2
       let index_lv1 = allLevelResult.lv1.map((f, index) => {
           if (f.idx === idx) {
               return index
           }
       })
       // get index of parent node
       index_lv1 = index_lv1.filter(v => v > -1)
       combineArray(lv1MapParent[idx], lv2MapParent[idx], index_lv1).map(obj =>{
           allLevelResult.lv2.push(obj)
       })
       // progess data for lv 3
    //    let index_lv2 = allLevelResult.lv2.map((f, index)=> {

    //    })
        // console.log(combineArray(lv2MapParent[idx], lv3MapParent[idx]))
       // progess data for lv 4

   })
//    console.log(allLevelResult)
console.log(department)
   })


// let a = [1,null,2, null]
// let b =[3,4,5,6]
// combineArray(a,b)