const crawler = require('crawler')
const fs = require('fs')
const pageMin = 1
const pageMax = 336
const sort = 'hdesc'
const searchLink = 'https://www.kkday.com/en/product/productlist/'
const puppeteer = require('puppeteer');
var pino = require('pino')()
const IsLogEnabled = true
const tours = require('./ToursData.json')
const resultFileName = "TourDetailInfo";
const groupTourSize = 1000

const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://root:root@localhost/mydb'
})

const cheerio  = require('cheerio')

const _ = require('lodash')


function custom_logging(message) {
    try {
        if (true) {
            pino.info(message);
        }
    } catch (error) {
        pino.error(new Error(error));
    }
}

async function LogErrors(error) {
    try {
        pino.error(new Error(error));
    } catch (err) {
        pino.error(new Error(err));
    }
}




function forceGC() {
    if (global.gc) {
        global.gc();
    } else {
        console.warn('No GC hook! Start your program as `node --expose-gc file.js`.');
    }

}

const insertDataToDB = async() => {
    // join all result file
    let files = []
    let tourDetailStore = []
    fs.readdirSync('./').forEach(file => {
        files.push(file)
    })
    files = files.filter(f => f.includes(resultFileName))
    tourDetailStore = await readMultiFile(files)
    tourDetailStore = _.flatten(tourDetailStore)
    tourDetailStore = tourDetailStore.map(t => {
        return {
            id: t.code,
            information: JSON.stringify(t)
        }

    })
    // insert it to database 
    await knex.batchInsert('tour_detail', tourDetailStore, 1000)
    console.log("done insert tour information")
}

const readMultiFile = (files) => {
    return Promise.all(files.map(f => {
        return new Promise((resolve, reject) => {
            // get data from file
            fs.readFile(f, (err, d) => {
                // convert json to obj
                d = JSON.parse(d)
                resolve(d.data)
            })
        })

    }))
}

const splitFileToFasterCrawler = () => {
    const links = tours.result.map(t => t.url)
    let arrUrl = _.chunk(links, groupTourSize);
    arrUrl = arrUrl.map((group, index) => {
        return group.map((link, i) => {
            return {
                uri: link,
                headers: {},
                callback: (err, res, done) => {
                    if (err) {
                        console.log(error)
                    } else {
                        const storeFileName = `${resultFileName}-${index}.json`
                        const $ = res.$
                        console.log("reading => " + link)
                        const includes = []
                        $('#home > div:nth-child(7) > div:nth-child(2) > div > ul').find('li').map((idx, el) => {
                            includes.push($(el).text().trim())
                        })
                        const excludes = []
                        $('#not_include > div:nth-child(2) > div > ul').find('li').map((idx, el) => {
                            excludes.push($(el).text().trim())
                        })
                        const imgs = []
                        $('#timetable > div:nth-child(2) > div').find('img').map((idx, el) => {
                            imgs.push($(el).attr('src'))
                        })
                        const videos = []
                        $('#video_area > div:nth-child(2)').find('iframe').map((idx, el) => {
                            videos.push($(el).attr('src'))
                        })
                        const importantInfo = []
                        $('#reminder > div:nth-child(2) > div > ul').find('li').map((idx, el) => {
                            importantInfo.push($(el).text().trim())
                        })
                        const itinerary = []

                        $('#timetable > div:nth-child(2) > div').find('.col-md-9').map((idx, el) => {
                            itinerary.push($(el).text().trim())
                        })
                        const guidLanguages = []
                        $('#guide_lang > div').find('img').map((idx, el) => {
                            guidLanguages.push($(el).attr('title'))
                        })
                        const stars = $('#comment_num').find('.text-primary').length
                        const instantBooking = $('body > div.productview > div.container.con-relative > div > div.col-md-7.productPage-detail > span > b').text()
                        console.log('name => ' + $('body > div.productview > div.container.con-relative > div > div.col-md-7.productPage-detail > h1').text())
                        let location = "",
                            duration = "",
                            description = ""
                        // detect instanctBooking
                        const cancellationPolicy = []
                        // $('#payway-text > div > div > div > div:nth-child(3) > table > tbody').find('tr').map((idx, tr) => {
                        //     // each cancellation deadline
                        //     const cancelDetail = {}
                        //     $(tr).find('td').map((idx, td) => {
                        //         if (idx == 0) {
                        //             cancelDetail.day = $(td).text()
                        //         }
                        //         if (idx == 1) {
                        //             cancelDetail.fee = $(td).text()
                        //         }
                        //     })
                        //     // retranslate 
                        //     cancelDetail.day = cancelDetail.day.match(/\d/g).join('-')
                        //     cancellationPolicy.push(cancelDetail)
                        // })
                        const reviewRoot = $('#comment_num > div').text().match(/\d/g)
                        const numerOfReviews = reviewRoot ? parseInt(reviewRoot.join('')) : 0
                        const currency = $('#priceContent > div > div.pull-right > h2 > span').text().trim()
                        const suggessPrice = $('#priceContent > div > div.pull-right > h2').text().match(/^\$?(\d{1,3},?(\d{3},?)*\d{3}(\.\d{0,2})?|\d{1,3}(\.\d{0,2})?|\.\d{1,2}?)$/)
                        console.log($('#priceContent > div > div.pull-right > h2').text())
                        if (instantBooking.trim().toLowerCase() == 'instant booking') {
                            location = $('body > div.productview > div.container.con-relative > div > div.col-md-7.productPage-detail > div:nth-child(4) > div > span').text(),
                                duration = $('body > div.productview > div.container.con-relative > div > div.col-md-7.productPage-detail > div:nth-child(5) > div > span').text(),
                                description = $('body > div.productview > div.container.con-relative > div > div.col-md-7.productPage-detail > div:nth-child(10) > div > span').text()
                        } else {
                            location = $('body > div.productview > div.container.con-relative > div > div.col-md-7.productPage-detail > div:nth-child(3) > div > span').text(),
                                duration = $('body > div.productview > div.container.con-relative > div > div.col-md-7.productPage-detail > div:nth-child(4) > div > span').text(),
                                description = $('body > div.productview > div.container.con-relative > div > div.col-md-7.productPage-detail > div:nth-child(9) > div > span').text()
                        }
                        forceGC()
                        fs.readFile(storeFileName, (err, data) => {
                            forceGC()
                            var json = JSON.parse(data)
                            json.data.push({
                                code: parseInt(link.split('/').pop()),
                                name: $('#prodInfo > h1').text(),
                                link,
                                location: location.trim().split('\n').pop().trim(),
                                duration: duration.trim().split('\n').pop().trim(),
                                description: $('#prodInfo > div.prod-intro').text(),
                                information: $('#home > div:nth-child(2) > div').text().trim(),
                                includes,
                                excludes,
                                voucher: $('#voucher_type > div:nth-child(2)').text().trim(),
                                imgs,
                                videos,
                                itinerary,
                                guidLanguages,
                                stars,
                                cancellationPolicy,
                                numerOfReviews,
                                // currency,
                                // suggessPrice
                            })
                            fs.writeFile(storeFileName, JSON.stringify(json), function (err) {
                                console.log("file appended!!!")
                                forceGC()
                            })
                        })
                        forceGC()
                        done()
                    }
                }
            }
        })
    })
    // run all group
    arrUrl.map((group, idx) => {
        // init data file
        var c = new crawler({
            maxConnections: 10,
            rateLimit: 10,
            // This will be called for each crawled page
            callback: function (error, res, done) {
                done()
            }
        });
        const initDataTemplate = {
            data: []
        }
        const storeFileName = `${resultFileName}-${idx}.json`
        fs.writeFileSync(storeFileName, JSON.stringify(initDataTemplate))
        c.queue(group)
    })
}
// insertDataToDB()


splitFileToFasterCrawler()

async function getNumPages(page) {
    try {
        const LENGTH_SELECTOR_CLASS = 'product-listview';
        let listLength = await page.evaluate((sel) => {
            return document.getElementsByClassName(sel).length;
        }, LENGTH_SELECTOR_CLASS);
        custom_logging('product listview Length - ' + listLength);

        const NUM_RESULT_SELECTOR = "#productListApp > div.row > main.col-md-9.product-list > div.product-listview--resultNumber > h4 > span";
        let inner = await page.evaluate((sel) => {
            let html = document.querySelector(sel).innerHTML;
            return html;
        }, NUM_RESULT_SELECTOR);
        //custom_logging('inner - '+inner);
        let results = parseInt(inner);
        let numPages = Math.ceil(results / listLength);
        custom_logging('Total Number of Pages() - ' + numPages + ' , Total results - > ' + results);
        return {
            totalItems: results,
            itemsPerPage: listLength,
            numOfPages: numPages
        };
    } catch (error) {
        var err = 'getNumPages - ' + error;
        await LogErrors(err);
        return {
            totalItems: 0,
            itemsPerPage: 0,
            numOfPages: 0,
            error: err
        };
    }
}

const list_all_availTours = async function (req, res) {
    var resultJson = {
        result: [],
        error: '',
        logid: ''
    };
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 0
    });
    try {
        const homePage = await browser.newPage();
        custom_logging('browser opened!');
        await homePage.goto(searchLink);
        await homePage.waitFor(2000);
        //Maximum navigation time in milliseconds, defaults to 30 seconds, pass 0 to disable timeout
        custom_logging('page opened! - ');

        let pageResult = await getNumPages(homePage);
        if (pageResult.totalItems > 0) {
            resultJson.itemsPerPage = pageResult.itemsPerPage;
            resultJson.totalItems = pageResult.totalItems;
            resultJson.numOfPages = pageResult.numOfPages;

            var listLength = pageResult.itemsPerPage + 2; //manually add 2 divs - product-listview--resultNumber,sorting              

            const productItem_Selector = '#productListApp > div.row > main.col-md-9.product-list > div:nth-child(INDEX) > a';
            const noOfResults_Selector = '#productListApp > div.row > main.col-md-9.product-list > div:nth-child(INDEX) > h4 > span';

            for (let p = 1; p <= resultJson.numOfPages; p++) {
                console.log("page -" + p)
                if (p > 1) {
                    await homePage.goto(`${searchLink}?page=${p}&sort=${sort}`);
                    await homePage.waitFor(200);
                }
                for (let i = 1; i <= listLength; i++) {
                    var prodDtlsJson = {
                        "url": "",
                        // "imageUrl": "",
                        // "name": "",
                        // "description": "",
                        // "product_place": "",
                        // "duration": "",
                        // "rating": "",
                        // "price": "",
                        // "currency": ""
                    };
                    try {
                        if (i === 1) {
                            let noOfItemsSelector = noOfResults_Selector.replace("INDEX", i);
                            let noofresults = await homePage.evaluate((sel) => {
                                return document.querySelector(sel).innerHTML;
                            }, noOfItemsSelector);
                            //custom_logging('no of results -> ' + noofresults);
                        }
                        if (i > 2) {
                            let urlSelector = productItem_Selector.replace("INDEX", i);

                            let url = await homePage.evaluate((sel) => {
                                let html = document.querySelector(sel);
                                if (html) {
                                    return document.querySelector(sel).getAttribute('href');
                                } else {
                                    return '';
                                }
                            }, urlSelector);

                            if (url && url !== '') {
                                custom_logging('product url -> ' + url + '->' + i);
                                prodDtlsJson.url = url;

                                // let image_url = await homePage.evaluate((sel) => {
                                //     if (document.querySelector(sel))
                                //         return document.querySelector(sel).getAttribute('style');
                                //     else return '';
                                // }, urlSelector + ' > div > div > div');

                                // custom_logging('product url -> ' + image_url + '->' + i);
                                // prodDtlsJson.imageUrl = image_url;

                                // let title = await homePage.evaluate((sel) => {
                                //     return (document.querySelector(sel)) ? document.querySelector(sel).innerText : '';
                                // }, urlSelector + ' > div > div:nth-child(2) > div.product-detail > h4');
                                // custom_logging('product url -> ' + title + '->' + i);
                                // prodDtlsJson.name = title

                                // let description = await homePage.evaluate((sel) => {
                                //     return (document.querySelector(sel)) ? document.querySelector(sel).innerText : '';
                                // }, urlSelector + ' > div > div:nth-child(2) > div.product-detail > p');
                                // custom_logging('product description -> ' + description + '->' + i);
                                // prodDtlsJson.description = description;

                                // let palce = await homePage.evaluate((sel) => {
                                //     return (document.querySelector(sel) && document.querySelector(sel).innerText) ?
                                //         document.querySelector(sel).innerText : '';
                                // }, urlSelector + ' > div > div:nth-child(2) > div.product-detail > div.product-place');
                                // custom_logging('product palce -> ' + palce + '->' + i);
                                // prodDtlsJson.product_place = palce;

                                // let duration = await homePage.evaluate((sel) => {
                                //     if (document.querySelector(sel) && document.querySelector(sel).innerText)
                                //         return document.querySelector(sel).innerText;
                                //     else return '';
                                // }, urlSelector + ' > div > div:nth-child(2) > div.product-detail > div.product-time');
                                // custom_logging('duration -> ' + duration + '->' + i);
                                // prodDtlsJson.duration = (duration) ? duration : '';

                                // let price = await homePage.evaluate((sel) => {
                                //     return (document.querySelector(sel) && document.querySelector(sel).innerText) ?
                                //         document.querySelector(sel).innerText : '';
                                // }, urlSelector + ' > div > div:nth-child(2) > div.product-detail > div.product-footer > div.product-pricing > h2');
                                // custom_logging('price -> ' + price + '->' + i);
                                // prodDtlsJson.price = price;

                                // let currency = await homePage.evaluate((sel) => {
                                //     return (document.querySelector(sel)) ? document.querySelector(sel).innerText : '';
                                // }, urlSelector + ' > div > div:nth-child(2) > div.product-detail > div.product-footer > div.product-pricing > h4');
                                // custom_logging('price -> ' + currency + '->' + i);
                                // prodDtlsJson.currency = currency;

                                custom_logging('------------------------------------');
                                if (i === 3) {
                                    // const dtlsPage = await browser.newPage();
                                    // getProductDtls('https://www.kkday.com/zh-hk/product/9912', dtlsPage);
                                    // getProductDtls(url, homePage);
                                }

                                resultJson.result.push(prodDtlsJson);
                            }
                        }
                    } catch (error) {
                        await LogErrors('product-list looping - ' + error);
                    }
                }
            }
        } else {
            resultJson.error = pageResult.error;
        }

    } catch (error) {
        var err = 'list_all_availTours - ' + error;
        await LogErrors(err);
        resultJson.error = err;
    }
    await browser.close();
    fs.writeFileSync('ToursData.json', JSON.stringify(resultJson))
    console.log("Tool Done")
}

// list_all_availTours()
