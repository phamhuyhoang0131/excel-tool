
const request = require('request-promise')
const Promise = require('bluebird')
const _ = require('lodash')
const fs = require('fs')
const numberOfTourInWorker = 1000
const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://root:root@localhost/mydb'
})
const firefox = require('selenium-webdriver/firefox')
const webdriver = require('selenium-webdriver/'),
    By = webdriver.By,
    until = webdriver.until;
const moment = require('moment')

const cheerio = require('cheerio')


const searchAvailTour = async ({
    starTime = '20180212',
    endTime = '20180216',
    cityCode = 'A01-003-00001',
    countryCode = 'A01-003',
    sort = 'hdesc'
}) => {
    console.time("progess time")
    const searchUrl = `https://www.kkday.com/en/product/ajax_productlist/${countryCode}?availstartdate=${starTime}&availenddate=${endTime}&sort=${sort}&city=${cityCode}`
    const tours = await request(searchUrl).then(res => JSON.parse(res))
    const {
        total,
        page,
        total_page
    } = tours
    // check when search have results 
    if (total > 0) {
        const firstPage = tours.data
        // request to get all tour avail
        let allTourAvail = []
        // start by 2 because we save first request
        for (let i = 2; i <= total_page; i++) {
            const pageRequest = `${searchUrl}&page= ${i}`
            allTourAvail.push(request(pageRequest).then(res => {
                res = JSON.parse(res)
                return res.data
            }))
        }
        allTourAvail = await Promise.all(allTourAvail)
        allTourAvail = _.flatten(firstPage.concat(allTourAvail))
        const tourIds = allTourAvail.map(t => parseInt(t.url_id))
        // get detail information from database 
        let tourDetailInfo = await knex('tour_detail_clone').whereIn('id', tourIds)
        console.log("số lượng tour detail => " + tourDetailInfo.length)
        const tourDetailIds = tourDetailInfo.map(t => t.id)
        const differenceId = _.difference(tourIds, tourDetailIds)
        // active crawler tool to get missing tour info 
        if (differenceId.length > 0) {
            await kkDayCrawlerV2(differenceId)
            // recompare search Tour Result and Tour Information get from db
            tourDetailInfo = await knex('tour_detail_clone').whereIn('id', tourIds)
            console.log("số lượng tour detail sau khi crawler lại => " + tourDetailInfo.length)
        }
        console.log("Số lượng tour gốc : => " + tourIds.length)
        // get tour options to caculate price
        const allProds = await Promise.all(tourDetailIds.map(id => {
            return getReleaseProdData(id)

        }))
        tourDetailInfo =  tourDetailInfo.map(t => {
            const productActivities = allProds.find(a => t.id == a.code)
            delete productActivities.code
            t.activities = productActivities.activities
            return t
        })
        console.log(allProds)
        fs.writeFileSync('tourSearchInfoFullDeTail.json', JSON.stringify(tourDetailInfo))
        // console.log(allProds)
        console.timeEnd("progess time")
    }
}

const getReleaseProdData = async (code, from = '20180212', to = '20180220') => {
    const getProdUrl = `https://www.kkday.com/en/product/ajax_get_product_related_data?prodOid=${code}&prodUrlOid=${code}&isPreview=false`
    let json = await request.get(getProdUrl)
    json = JSON.parse(json)
    const dates = getDates(from, to)

    let {
        pkgList,
        pkg_calendar,
        is_sold_out,
        package_status,
        package_status_message,
        event_data,
    } = json.data
    const pkgArr = []
    let availDates = []
    for (const key in pkg_calendar) {
        if (pkg_calendar.hasOwnProperty(key)) {
            const element = pkg_calendar[key];
            const inRange = dates.includes(key)
            if (inRange) {
                availDates.push({
                    code,
                    date: key,
                    pkgTime: event_data
                })
            }
        }
    }
    const activities = []
    for (const key in pkgList) {
        if (pkgList.hasOwnProperty(key)) {
            const package = pkgList[key];
            // get ages limited
            let priceList = []
            const {minOrderNum, maxOrderNum, minOrderQty, orderQty, unitTxt} = package
            const isTraveler = (unitTxt == 'Traveler')

            if(package.price1USD){
                // price adult common

                priceList.push({
                    price: package.price1USD,
                    type: 'adult'
                })
            }
            if(package.price2USD){
                // price child common
                priceList.push({
                    price: package.price2USD,
                    type: 'child'
                })
            }
            if(package.price3USD){
                // price infant
                priceList.push({
                    price: package.price3USD,
                    type: 'infant'
                })
            }
            if(package.price4USD){
                // price for old people
                priceList.push({
                    price: package.price4USD,
                    type: 'old'
                })
            }
            priceList = priceList.map(p => {
                if(!isTraveler){
                    delete p.type
                }
                p.minOrderNum = minOrderNum
                p.maxOrderNum = maxOrderNum
                p.minOrderQty = minOrderNum
                // p.orderQty = orderQty
                p.unitName = unitTxt
                return p
            })

            let template = {
                name: package.pkgDesc,
                code: package.pkgOid,
                dailyRates: [],
                priceList
            }
            // progress dailyRates

            const rates = availDates.map(d => {
                let {
                    date,
                    pkgTime
                } = d
                const matchDate = pkg_calendar[date]
                // when date is valid
                if(matchDate){
                    const matchPackagesId = matchDate.find( p => p == key)
                    if(matchPackagesId){
                        // to do : get time from source
                        let times = []
                        if(pkgTime){
                            // find by packages ID
                            times = pkgTime[key] ? pkgTime[key][date] : []
                        }
                        template.dailyRates.push({
                            date,
                            times
                        })

                    }
                }
            })
            activities.push(template)

        }
    }
    return  {
        code,
        activities
    } 

}


searchAvailTour({})

const objToArr = (object) => {
    let arr = []
    for (const key in object) {
        if (object.hasOwnProperty(key)) {
            const element = object[key];
            arr.push({
                id: key,
                body: element
            })
        }
    }
    return arr
}

function getDates(startDate, stopDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push(moment(currentDate).format('YYYYMMDD'))
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}


const kkDayCrawlerV2 = async (sources) => {

    sources = sources.map(s => {
        return `https://www.kkday.com/en/product/${s}`
    })
    sources = _.chunk(sources, numberOfTourInWorker)
    const workers = await Promise.all(sources.map((s, idx) => {
        return KKDayCrawlerWorker(s, idx)
    }))
    console.log("done crawler data!!!")
}




const KKDayCrawlerWorker = async (sources, idx) => {
    const dataHub = []
    var firefoxOptions = new firefox.Options();
    firefoxOptions.headless();
    var driverWorker = new webdriver.Builder()
        .forBrowser('firefox')
        .setFirefoxOptions(firefoxOptions)
        .build();

    for (let i = 0; i < sources.length; i++) {
        const link = sources[i]
        // progess for each link
        await driverWorker.get(link)
        await driverWorker.wait(until.elementLocated(By.tagName("body")), 100)
        const body = await driverWorker.findElement(By.css("body")).getAttribute("innerHTML")
        const $ = cheerio.load(body)
        const tourName = $('#prodInfo > h1').text()
        let instantBooking = $('#prodInfo > span').text() || ''
        let duration,
            location,
            guideLanguages = [],
            descriptions,
            reivewStar,
            imgs = [],
            experience,
            itineraryTexts = [],
            itineraryImgs = [],
            video,
            includes = [],
            excludes = [],
            reminder = [],
            voucher = [],
            cancellations = [],
            mettingMap = [],
            experienceMap = [];
        $('#map-meeting').find('li').map((idx, m) => {
            const locationName = $(m).find('span').text()
            const img = $(m).find('img').attr("src")
            const locationPoint = $(m).find('a').attr("href")
            mettingMap.push({
                name: locationName,
                img,
                point: locationPoint
            })
        })
        $('#map-purpose').find('li').map((idx, m) => {
            const locationName = $(m).find('span').text()
            const img = $(m).find('img').attr("src")
            const locationPoint = $(m).find('a').attr("href")
            experienceMap.push({
                name: locationName,
                img,
                point: locationPoint
            })
        })
        instantBooking = (instantBooking.trim().toLowerCase() == "instant booking")
        if (instantBooking) {
            location = $('#prodInfo > div:nth-child(4)').text();
            duration = $('#prodInfo > div:nth-child(5)').text();
        } else {
            location = $('#prodInfo > div:nth-child(3)').text();
            duration = $('#prodInfo > div:nth-child(4)').text();
        }
        $('#prodInfo > div.critical-info.guide-lang > div').find('img').map((idx, el) => {

            guideLanguages.push($(el).attr('data-original-title'))
        })
        descriptions = $('#prodInfo > div.prod-intro').text()
        reivewStar = $('#review-num').find(".text-primary").length
        $('.owl-lazy').map((idx, el) => {
            imgs.push($(el).attr("data-src"))
        })
        experience = $('#home > div:nth-child(2) > div').text()
        $('.txt').map((idx, el) => {
            itineraryTexts.push($(el).text())
        })
        $('#timetable').find('img').map((idx, el) => {
            itineraryImgs.push($(el).attr('src'))
        })
        video = $('#video-area').find('iframe').attr("src")
        $('#reminder').find('li').map((idx, el) => {
            reminder.push($(el).text())
        })
        $('#voucher-type').find('li').map((idx, el) => {
            voucher.push($(el).text())
        })
        // detect includes
        const arrIds = [3, 4, 5, 6]
        for (let index = 0; index < arrIds.length; index++) {
            const i = arrIds[index];
            const element = $(`#home > div:nth-child(${i}) > h3`)
            if (element.text().trim().toLowerCase() == 'includes') {
                // get includes info
                $(`#home > div:nth-child(${i}) > div > ul`).find('li').map((idx, el) => {
                    includes.push($(el).text().trim())
                })
                // because excludes information is next includes 
                const excludesIndex = i + 1
                // get excludes info
                $(`#home > div:nth-child(${excludesIndex}) > div > ul`).find('li').map((idx, el) => {
                    excludes.push($(el).text().trim())
                })
                break;
            }
        }
        const tourInfo = {

            link,
            code: parseInt(link.split('/').pop()),
            tourName,
            duration,
            location,
            instantBooking,
            guideLanguages,
            descriptions,
            reivewStar,
            imgs,
            experience,
            itineraryTexts,
            itineraryImgs,
            video,
            reminder,
            voucher,
            excludes,
            includes,
            mettingMap,
            experienceMap
        }
        console.log(tourInfo.link)
        // insert to db
        await knex('tour_detail_clone').insert({
            id: tourInfo.code,
            information: JSON.stringify(tourInfo)
        })
        // write info to file
        forceGC()
    }
    await driverWorker.close()
}


function forceGC() {
    if (global.gc) {
        global.gc();
    } else {
        console.warn('No GC hook! Start your program as ``.');
    }

}