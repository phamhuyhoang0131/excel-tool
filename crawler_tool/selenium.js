var webdriver = require('selenium-webdriver/'),
  By = webdriver.By,
  until = webdriver.until,
  Promise = require('bluebird'),
  _ = require('lodash');
const csv = require('fast-csv')

const request = require('request-promise')
const firefox = require('selenium-webdriver/firefox')

const moment = require('moment')

const tours = require('./ToursData.json')
const numberOfTourInWorker = 1800
const DataFileName = 'tourDataV2'
const fs = require('fs')
var driver = new webdriver.Builder()
  .forBrowser('firefox')
  .build();
const knex = require('knex')({
  client: 'mysql2',
  connection: 'mysql://root:root@localhost/mydb'
})

function readCsv(path) {
  return new Promise((resolve, reject) => {
    let csv_arr = []
    fs.createReadStream(path)
      .pipe(csv())
      .on("data", function (data) {
        csv_arr.push(data)
      })
      .on("end", function () {
        resolve(csv_arr)
      });
  })

}

const cheerio = require('cheerio')

const testReq = 'https://www.kkday.com/en/product/productlist/A01-003?page=1&city=A01-003-00001&availstartdate=20180210&availenddate=20180221&sort=hdesc'

const seleniumDriver = async(url) => {
  // wait to page load completed
  await driver.get(url)
  // wail to main product view complate 
  let numberOfResults = await driver.findElement(By.className('product-listview--resultNumber')).findElement(By.tagName('h4')).findElement(By.className('text-primary'))
  numberOfResults = await numberOfResults.getText()
  numberOfResults = parseInt(numberOfResults.trim())
  const numberOfPages = numberOfResults > 0 ? Math.ceil(numberOfResults / 10) + 1 : 0
  console.log("số lượng page kết quả: => " + numberOfPages)
  // serial all page
  let allPages = []
  for (let i = 1; i < numberOfPages; i++) {
    let page = `${url}&page=${i}`
    await driver.get(page)
    const mainView = await driver.findElements(By.className('product-listview'))
    const allProduct = await Promise.all(mainView.map(el => {
      return el.getAttribute('innerHTML').then(html => {
        const $ = cheerio.load(html)
        const link = $('a').attr('href')
        const [currency, seprate, price] = $('.product-pricing').text().split(" ")
        return {
          link,
          price,
          currency
        }
      })
    }))
    allPages.push(allProduct)
  }
  allPages = _.flatten(allPages)
  await driver.close()
  return allPages

}




const login = async(url) => {
  await driver.get("https://www.kkday.com/en")
  const isLoggedVisiable = await driver.findElements(By.css('header-main-sidenav-button'))
  if (isLoggedVisiable.length == 0) {
    await driver.findElement(By.css('div.table-cell:nth-child(4) > button:nth-child(1)')).click()
    // click button login
    await driver.findElement(By.id('loginEmail')).sendKeys("hoangph@goquo.com")
    await driver.findElement(By.id('loginPassword')).sendKeys('hoang123')
    await driver.findElement(By.id('loginBtn')).click()
    await driver.navigate().refresh()
    console.log("refreshing")
    const isLogged = await driver.findElements(By.id('header-main-sidenav-button'))
    if (isLogged.length > 0) {
      console.log("success login")
      return true
    }
    return false
  }
  console.log("is logged")
  return true
}

// login()

/**
 * 
 * @param {*} startTime : beginning of Trip 
 * @param {*} endTime : end of Trip
 * @param {*} sort : sort type 1: desc , 2 : asc  
 * @param {*} language : tour language 
 */
const buidlReq = (startTime, endTime, sort, language, countryCode, cityCode) => {
  // const page = 1
  const sortOptions = ['sdesc', 'pasc', 'pdesc', 'sasc']
  const languages = ['日本語', 'English', '中文', '한국어', '廣東話', 'Français']
  const url = `https://www.kkday.com/en/product/productlist/${countryCode}/?city=${cityCode}&availstartdate=${startTime}&availenddate=${endTime}&sort=${sortOptions[0]}`
  return url
}



const prebook = async(url) => {
  await driver.get('https://www.kkday.com/en/product/9912')
}

const buildPrebookRequest = async(code) => {
  const req = `https://www.kkday.com/en/product/ajax_get_product_related_data?prodOid=${code}&prodUrlOid=${code}&isPreview=true`
  let json = await request.get(req)
  json = JSON.parse(json)
  let {
    pkgList,
    pkg_calendar,
    is_sold_out,
    package_status,
    package_status_message,
    event_data
  } = json.data
  const pkgArr = []
  for (const key in pkgList) {
    if (pkgList.hasOwnProperty(key)) {
      const pkg = pkgList[key];
      pkgArr.push({
        pkgId: key,
        content: pkg
      })

    }
  }
  return {
    code,
    is_sold_out,
    package_status,
    package_status_message,
    pkg_calendar,
    pkgList: pkgArr,
    pkgTime: event_data
  }

}


// buildPrebookRequest(1936)



const progess = async() => {
  console.time("search Time")
  const url = buidlReq('20180214', '20180219', 'hdesc', 'English', 'A01-010', 'A01-010-00001')
  let searchResults = await seleniumDriver(url)

  searchResults = searchResults.map(s => {
    s.id = parseInt(s.link.split('/').pop())
    return s
  })
  console.log("số tour search được : =>" + searchResults.length)
  console.log("Tour đầu tiên: => " + JSON.stringify(searchResults[0]))
  // get tour information from db
  let tourInfor = await knex('tour_detail_clone').whereIn('id', searchResults.map(s => s.id))
  console.log("Số tour info trong db => " + tourInfor.length)
  // need handle some tour infor missing when crawler 
  console.log(_.difference(tourInfor.map(t => t.id), searchResults.map(s => s.id)))
  tourInfor = tourInfor.map(t => {
    const matchTour = searchResults.find(s => s.id == t.id)
    t.information.currency = matchTour.currency
    t.information.price = matchTour.price
    return t
  })
  // get more tour options information 
  const packagesData = await Promise.all(tourInfor.map(t => t.id).map(t => {
    return buildPrebookRequest(t)
  }))

  // make activities
  let activities
  activities = packagesData.filter(p => p.is_sold_out == false).map(p => {
    let {
      code,
      is_sold_out,
      package_status,
      package_status_message,
      pkg_calendar,
      pkgList,
      pkgTime
    } = p
    let availDates = []
    const dates = getDates('20180214', '20180219')
    for (const key in pkg_calendar) {
      if (pkg_calendar.hasOwnProperty(key)) {
        const element = pkg_calendar[key];
        const inRange = dates.includes(key)
        if (inRange) {
          availDates.push({
            code,
            date: key,
            packagesIds: element,
            pkgTime
          })
        }
      }
    }
    availDates = availDates.map(d => {
      const {
        code,
        date,
        packagesIds,
        pkgTime
      } = d
      let matchPKG = pkgList.filter(p => packagesIds.includes(p.pkgId))
      matchPKG.map
      return {
        date,
        matchPKG,
        code
      }
    })
    pkgTime
    return availDates
  })
  activities = _.flatten(activities)
  activities = _.groupBy(activities, 'code')
  console.log(activities)
  console.log("số lượng tour => "+ Object.keys(activities).length)

  console.timeEnd("search Time")
}




progess()


function getDates(startDate, stopDate) {
  var dateArray = [];
  var currentDate = moment(startDate);
  var stopDate = moment(stopDate);
  while (currentDate <= stopDate) {
    dateArray.push(moment(currentDate).format('YYYYMMDD'))
    currentDate = moment(currentDate).add(1, 'days');
  }
  return dateArray;
}



const kkDayCrawlerV2 = async() => {
  let sources = tours.result.map(t => t.url)
  let sourceIdsContinue = await readCsv('/home/hoangph/src/excel-tool/static /ids.csv')
  sourceIdsContinue.shift()
  sourceIdsContinue = sourceIdsContinue
  sourceIdsContinue = _.flatten(sourceIdsContinue)
  const sourceIds = sources.map(s => {
    return s.split('/').pop()
  })
  sources = _.difference(sourceIds, sourceIdsContinue)
  sources = sources.map(s => {
    return `https://www.kkday.com/en/product/${s}`
  })
  sources = _.chunk(sources, numberOfTourInWorker)
  const workers = await Promise.all(sources.map((s, idx) => {
    return KKDayCrawlerWorker(s, idx)
  }))
  console.log("done crawler data!!!")
}




const KKDayCrawlerWorker = async(sources, idx) => {
  const dataHub = []

  var firefoxOptions = new firefox.Options();
  // firefoxOptions.setBinary('/home/hoangph/Users/me/geckodriver');
  firefoxOptions.headless();

  var driverWorker = new webdriver.Builder()
    .forBrowser('firefox')
    .setFirefoxOptions(firefoxOptions)
    .build();
  const storeFileName = `${DataFileName}-${idx}.json`

  for (let i = 0; i < sources.length; i++) {
    const link = sources[i]
    // progess for each link
    await driverWorker.get(link)
    await driverWorker.wait(until.elementLocated(By.tagName("body")), 100)
    const body = await driverWorker.findElement(By.css("body")).getAttribute("innerHTML")
    const $ = cheerio.load(body)
    const tourName = $('#prodInfo > h1').text()
    let instantBooking = $('#prodInfo > span').text() || ''
    let duration,
      location,
      guideLanguages = [],
      descriptions,
      reivewStar,
      imgs = [],
      experience,
      itineraryTexts = [],
      itineraryImgs = [],
      video,
      includes = [],
      excludes = [],
      reminder = [],
      voucher = [],
      cancellations = [],
      mettingMap = [],
      experienceMap = [];
    $('#map-meeting').find('li').map((idx, m) => {
      const locationName = $(m).find('span').text()
      const img = $(m).find('img').attr("src")
      const locationPoint = $(m).find('a').attr("href")
      mettingMap.push({
        name: locationName,
        img,
        point: locationPoint
      })
    })
    $('#map-purpose').find('li').map((idx, m) => {
      const locationName = $(m).find('span').text()
      const img = $(m).find('img').attr("src")
      const locationPoint = $(m).find('a').attr("href")
      experienceMap.push({
        name: locationName,
        img,
        point: locationPoint
      })
    })
    instantBooking = (instantBooking.trim().toLowerCase() == "instant booking")
    if (instantBooking) {
      location = $('#prodInfo > div:nth-child(4)').text();
      duration = $('#prodInfo > div:nth-child(5)').text();
    } else {
      location = $('#prodInfo > div:nth-child(3)').text();
      duration = $('#prodInfo > div:nth-child(4)').text();
    }
    $('#prodInfo > div.critical-info.guide-lang > div').find('img').map((idx, el) => {

      guideLanguages.push($(el).attr('data-original-title'))
    })
    descriptions = $('#prodInfo > div.prod-intro').text()
    reivewStar = $('#review-num').find(".text-primary").length
    $('.owl-lazy').map((idx, el) => {
      imgs.push($(el).attr("data-src"))
    })
    experience = $('#home > div:nth-child(2) > div').text()
    $('.txt').map((idx, el) => {
      itineraryTexts.push($(el).text())
    })
    $('#timetable').find('img').map((idx, el) => {
      itineraryImgs.push($(el).attr('src'))
    })
    video = $('#video-area').find('iframe').attr("src")
    $('#reminder').find('li').map((idx, el) => {
      reminder.push($(el).text())
    })
    $('#voucher-type').find('li').map((idx, el) => {
      voucher.push($(el).text())
    })
    // detect includes
    const arrIds = [3, 4, 5, 6]
    for (let index = 0; index < arrIds.length; index++) {
      const i = arrIds[index];
      const element = $(`#home > div:nth-child(${i}) > h3`)
      if (element.text().trim().toLowerCase() == 'includes') {
        // get includes info
        $(`#home > div:nth-child(${i}) > div > ul`).find('li').map((idx, el) => {
          includes.push($(el).text().trim())
        })
        // because excludes information is next includes 
        const excludesIndex = i + 1
        // get excludes info
        $(`#home > div:nth-child(${excludesIndex}) > div > ul`).find('li').map((idx, el) => {
          excludes.push($(el).text().trim())
        })
        break;
      }
    }
    const tourInfo = {

      link,
      code: parseInt(link.split('/').pop()),
      tourName,
      duration,
      location,
      instantBooking,
      guideLanguages,
      descriptions,
      reivewStar,
      imgs,
      experience,
      itineraryTexts,
      itineraryImgs,
      video,
      reminder,
      voucher,
      excludes,
      includes,
      mettingMap,
      experienceMap
    }
    console.log(tourInfo.link)
    // insert to db
    await knex('tour_detail_clone').insert({
      id: tourInfo.code,
      information: JSON.stringify(tourInfo)
    })
    // write info to file
    forceGC()
  }

  await driverWorker.close()
}


function forceGC() {
  if (global.gc) {
    global.gc();
  } else {
    console.warn('No GC hook! Start your program as ``.');
  }

}
// kkDayCrawlerV2()