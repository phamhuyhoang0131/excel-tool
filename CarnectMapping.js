const transform  = require('camaro')
const promise = require('bluebird')
const builder = require('xmlbuilder')
const _ = require('lodash')
const request = promise.promisifyAll(require('request'))
const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://root:@localhost/rentalCars'
})

// knex('carnect_mapping').then(console.log)
const randomstring  = require('randomstring')
const fs = require('fs')


const makeRequest = (reqObj, action) => {
    const req = {
        headers: {
            'Content-Type': 'text/xml; charset=utf-8',
            'SOAPAction': `http://www.opentravel.org/OTA/2003/05/${action}`
        },
        body: reqObj,
        method: 'POST',
        uri: 'http://ota2007a.carhire-solutions.com/destination.asmx'
    }
    return request.postAsync(req).then(res => res.body)
}

const buildRequest = (action, data) => {
    let requestObj
    switch (action) {
        case 'GetCountries':
            requestObj = `<x:Envelope xmlns:x=\"http:\/\/schemas.xmlsoap.org\/soap\/envelope\/\" xmlns:ns=\"http:\/\/www.opentravel.org\/OTA\/2003\/05\">\r\n    <x:Header\/>\r\n    <x:Body>\r\n        <ns:VehicleCountryRequest>\r\n            <ns:Language>EN<\/ns:Language>\r\n        <\/ns:VehicleCountryRequest>\r\n    <\/x:Body>\r\n<\/x:Envelope>`
            break;
        case 'GetCities':
            requestObj = `<x:Envelope xmlns:x=\"http:\/\/schemas.xmlsoap.org\/soap\/envelope\/\" xmlns:ns=\"http:\/\/www.opentravel.org\/OTA\/2003\/05\">\r\n    <x:Header\/>\r\n    <x:Body>\r\n        <ns:VehicleCityRequest>\r\n            <ns:CountryID>${data.id}<\/ns:CountryID>\r\n            <ns:Language>EN<\/ns:Language>\r\n        <\/ns:VehicleCityRequest>\r\n    <\/x:Body>\r\n<\/x:Envelope>`
            break;
        case 'GetAirports':
            requestObj = `<x:Envelope xmlns:x=\"http:\/\/schemas.xmlsoap.org\/soap\/envelope\/\" xmlns:ns=\"http:\/\/www.opentravel.org\/OTA\/2003\/05\">\r\n    <x:Header\/>\r\n    <x:Body>\r\n        <ns:VehicleAirportRequest>\r\n            <ns:Language>EN<\/ns:Language>\r\n            <ns:CountryID>${data.id}<\/ns:CountryID>\r\n        <\/ns:VehicleAirportRequest>\r\n    <\/x:Body>\r\n<\/x:Envelope>`
            break;
        default:
            break;
    }
    return requestObj
}

const finalRequest = (action, data) => {
    const req = buildRequest(action, data)
    return makeRequest(req, action)
} 

const process = async () => {
    // step one: get all countries data 
    let countries = await finalRequest('GetCountries')
    countries = transform(countries, {
        country: ['//Countries/Country', {
            code: '@id',
            ISO: 'ISO'
        }]
    }).country
    // step two: get all airports by countries
    let airports = countries.map(country => {
        return finalRequest('GetAirports', { id: country.code }).then(data => {
            return transform(data, {
                airport: ['//Airports/Airport', {
                    id: '@id',
                    name: 'Name',
                    code: '@iata',
                    lat: '@latitude',
                    long: '@longitude',
                    country: `#${country.ISO}`
                }]
            }).airport
        })
    })
    airports = await promise.all(airports)

    airports =  _.flatten(airports)
    // insert to db
    await knex.batchInsert('carnect_mapping', airports, 500)
    console.log('insert complete')
}


process().then()

