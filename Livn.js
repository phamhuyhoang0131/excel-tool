// const transform = require('camaro')
const promise = require('bluebird')
const builder = require('xmlbuilder')
const _ = require('lodash')
const request = promise.promisifyAll(require('request'))
const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://root:@localhost/rentalCars'
})
const transform = require('jsonpath-object-transform')
const base64 = require('base-64')

const fs = require('fs')
const airpors = require('./static /LinvAirportList.json')

const getAirpors = async() => {
    let options = {
        url: 'https://dev1.livngds.com/livngds/api/products/airports',
        headers: {
            Authorization: 'Basic R29RdW86dFRKV000cTgyM19zWDRO',
            Accept: 'application/json'
        },
        json: true
    }
    return request.getAsync(options)
}

const getAllTour = async() => {
    const allRequest = airpors.List.map(a => {
        const searchCondition = `${a.iata},50`
        let options = {
            url: `https://dev1.livngds.com/livngds/api/products/search?includeFullDetails=true&airport=${searchCondition}&matchAll=false&matchPartial=true&includeDisabled=false&order=id&includeOptions=true&treeView=false&requiredUnits=1&startDate=2017-12-10&endDate=2017-12-15`,
            headers: {
                Authorization: 'Basic R29RdW86dFRKV000cTgyM19zWDRO',
                Accept: 'application/json'
            },
            json: true
        }
        return request.getAsync(options).then(res => res.body)
    })
    let allResponse = await promise.all(allRequest)
    // map airport with results
    let tours = airpors.List.map((airport, idx) => {
        return {
            airportCode: airport.iata,
            numberTour: allResponse[idx].List.length
        }
    })
    tours = tours.filter(t => t.numberTour > 0)
    fs.writeFileSync('tour.json', JSON.stringify(tours))
    console.log("done")
    // return allResponse.shift()

}

// getAllTour().then(info => {

// })

// console.log(base64.decode('R29RdW86dFRKV000cTgyM19zWDRO'))

// console.log(base64.encode('GoQuo:tTJWM4q823_sX4N'))

const template = {
    bookingReference: '',
    providerReference: '$.cart.reservations[0].id',
    bookingDate: '$.cart.reservations[0].confirmed',
    status: '$.cart.reservations[0].status',
    tourCode: '$.cart.reservations[0].reservationItems[0].productCode',
    name: '$.cart.reservations[0].reservationItems[0].productName',
    airportCode: '',
    date: '$.cart.reservations[0].productDate',
    deptPoint: '$.cart.reservations[0].reservationItems[0].pickupPointName',
    deptName: '$.cart.reservations[0].reservationItems[0].pickupAddress',
    deptTime: '$.cart.reservations[0].reservationItems[0].pickupTime',
    currencyCode: '$.cart.reservations[0].operatorCurrency',
    amount: '$.cart.reservations[0].grandTotalRetailOverride',
    cancellationPolicies: []
}

const source = {
    "cart": {
        "id": 5558,
        "expired": false,
        "status": "COMPLETED",
        "dateAvAndRatesChecked": "2017-11-07T18:55:45.704+11:00",
        "dateSold": "2017-11-07T18:55:53.094+11:00",
        "currency": "AUD",
        "retailCurrency": "AUD",
        "paxes": [{
                "id": 8713,
                "age": 30,
                "gender": "M",
                "salutation": "MR",
                "firstName": "Magento",
                "lastName": "Max",
                "address1": "",
                "email": "anhthang@goquo.com",
                "phone": "1231230123980",
                "mobile": "1231230123980"
            },
            {
                "id": 8714,
                "age": 2,
                "gender": "F",
                "salutation": "MS",
                "firstName": "Javis",
                "lastName": "Cen",
                "address1": ""
            },
            {
                "id": 8715,
                "age": 4,
                "gender": "F",
                "salutation": "MS",
                "firstName": "Javis",
                "lastName": "Thor",
                "address1": ""
            }
        ],
        "cartItems": [{
                "id": 9130,
                "paxIndex": 0,
                "retailRef": "GoQuo",
                "productId": 900,
                "productCid": 243,
                "productDate": "2017-12-10",
                "productName": "1D Great Ocean Road Tour Classic  - 1D Great Ocean Road Tour Classic",
                "pickupId": 7996,
                "pickupTime": "07:15:00.000",
                "pickupPointName": "Bunyip Tours Reception",
                "pickupPointAddress": "570 Flinders St, Melbourne, VIC, 3000",
                "retailRate": 130,
                "currency": "AUD",
                "handlingSurchargePercentage": 4,
                "leviesTotal": 0,
                "levies": [{
                    "amount": 0,
                    "description": "Levy Adult"
                }],
                "cancellationPolicy": [{
                        "hoursTillDeparture": 24,
                        "feePerc": 100
                    },
                    {
                        "hoursTillDeparture": 48,
                        "feePerc": 50
                    },
                    {
                        "hoursTillDeparture": 8760,
                        "feePerc": 25
                    }
                ],
                "operatorCurrency": "AUD",
                "operatorRate": 130,
                "retailCommissionPerc": 0,
                "wholesaleCommissionAmount": 39,
                "duration": 86400000,
                "operatorId": 43,
                "operatorCid": 22,
                "wholesaleNetRate": 91
            },
            {
                "id": 9131,
                "paxIndex": 1,
                "retailRef": "GoQuo",
                "productId": 900,
                "productCid": 243,
                "productDate": "2017-12-10",
                "productName": "1D Great Ocean Road Tour Classic  - 1D Great Ocean Road Tour Classic",
                "pickupId": 7996,
                "pickupTime": "07:15:00.000",
                "pickupPointName": "Bunyip Tours Reception",
                "pickupPointAddress": "570 Flinders St, Melbourne, VIC, 3000",
                "retailRate": 95,
                "currency": "AUD",
                "handlingSurchargePercentage": 4,
                "leviesTotal": 0,
                "levies": [{
                    "amount": 0,
                    "description": "Levy Child"
                }],
                "cancellationPolicy": [{
                        "hoursTillDeparture": 24,
                        "feePerc": 100
                    },
                    {
                        "hoursTillDeparture": 48,
                        "feePerc": 50
                    },
                    {
                        "hoursTillDeparture": 8760,
                        "feePerc": 25
                    }
                ],
                "operatorCurrency": "AUD",
                "operatorRate": 95,
                "retailCommissionPerc": 0,
                "wholesaleCommissionAmount": 28.5,
                "duration": 86400000,
                "operatorId": 43,
                "operatorCid": 22,
                "wholesaleNetRate": 66.5
            },
            {
                "id": 9132,
                "paxIndex": 2,
                "retailRef": "GoQuo",
                "productId": 900,
                "productCid": 243,
                "productDate": "2017-12-10",
                "productName": "1D Great Ocean Road Tour Classic  - 1D Great Ocean Road Tour Classic",
                "pickupId": 7996,
                "pickupTime": "07:15:00.000",
                "pickupPointName": "Bunyip Tours Reception",
                "pickupPointAddress": "570 Flinders St, Melbourne, VIC, 3000",
                "retailRate": 95,
                "currency": "AUD",
                "handlingSurchargePercentage": 4,
                "leviesTotal": 0,
                "levies": [{
                    "amount": 0,
                    "description": "Levy Child"
                }],
                "cancellationPolicy": [{
                        "hoursTillDeparture": 24,
                        "feePerc": 100
                    },
                    {
                        "hoursTillDeparture": 48,
                        "feePerc": 50
                    },
                    {
                        "hoursTillDeparture": 8760,
                        "feePerc": 25
                    }
                ],
                "operatorCurrency": "AUD",
                "operatorRate": 95,
                "retailCommissionPerc": 0,
                "wholesaleCommissionAmount": 28.5,
                "duration": 86400000,
                "operatorId": 43,
                "operatorCid": 22,
                "wholesaleNetRate": 66.5
            }
        ],
        "reservations": [{
            "id": 3256,
            "globalRef": "101-1988",
            "cartRetailRef": null,
            "cancellationQuote": "https://dev1.livngds.com/livngds/api/reservations/3256/cancellationQuote",
            "idExternal": "-999999",
            "operatorId": 43,
            "operatorCid": 22,
            "operatorCurrency": "AUD",
            "productDate": "2017-12-10",
            "retailCurrency": "AUD",
            "handlingSurchargePercentage": 4,
            "grandTotalOperator": 320,
            "grandTotalRetailOverride": 320,
            "retailCommissionTotal": 0,
            "wholesaleCommissionTotal": 96,
            "numberOfPax": 3,
            "tnc": "<h2>Bunyip Tours:</h2>\n<p><strong>1. Our Vehicles</strong><br />\nOur tours are conducted in modern air-conditioned vehicles (Range in size from 13 -24 seats). With small tour groups, there is plenty of time to get out &amp; explore nature at its very best. You will enjoy the benefits of personalized &amp; interactive service with our experienced, enthusiastic and informative tour guides.</p>\n\n<p><strong>2. Our Main Departure Point</strong><br />\nOur main tour departure center is centrally located in the Melbourne Central Business District at 570 Flinders Street and easily accessible by public transport.&nbsp; Commencing 1st January 2015 all tram travel within the city is FREE and with a tram stop right outside our front door, accessing our office has never been easier (or cheaper!).&nbsp; If catching a train, we are located an easy 2 minute walk from Southern Cross Station.&nbsp; Just head south towards Flinders Street.&nbsp; We are located on the corner of Flinders and Spencer Street (just next door to the 7Eleven Convenience Store).</p>\n\n<p><strong>3. Free Courtesy Pick Up</strong><br />\nShould it be required, we offer our customers a courtesy pick-up &amp; drop off service from most inner city accommodations<br />\n(excludes Neighbours Tours).&nbsp; Please confirm your allocated hotel / hostel pick up time when making your booking.<br />\nPlease wait outside your accommodation 5 minutes prior to pick-up time to avoid delays and we will transfer you to the main departure point free of charge.&nbsp; Please note pick-up times may vary slightly depending on traffic, but we are unable to wait for lengthy periods of time should you be delayed.&nbsp; You may have to make your own way to the main departure point or miss your tour so make sure to look out for us (no refunds for missed departures!).</p>\n\n<p><strong>4. Luggage Restriction</strong><br />\nOnly small carry bags on one day tours<br />\n5 kg soft bag on two &amp; three day tours returning to Melbourne<br />\n15 kg backpack for three day tours with an Adelaide connection<br />\n5 kg carry-on bag for Jump on / Jump off option</p>\n\n<p><strong>5. Child Policy</strong><br />\nDue to the duration and nature of our day and extended tours, we do not believe they are suitable for children under 4 years of age. However please feel free to contact our friendly staff for recommendations, as there are a number of tour operators that will be able to assist should you be travelling with very young children.&nbsp; For children 4 to 12 years of age child rates apply.&nbsp; Please note that if you are travelling with children on one of our extended tours you will need to chose the private room option as dorm style accommodation can not be offered as it is not suitable.</p>\n\n<p><strong>6. Tour prices &amp; Itineraries</strong><br />\nAddictive Entertainment &amp; Tours P/L t/as BUNYIP TOURS retains the right to alter itineraries, fares and vehicles and may cancel tours in unforeseen circumstances.&nbsp; Importantly BUNYIP TOURS reserves the right to use other accredited tour<br />\noperators and larger coaches during peak periods if required.</p>\n\n<p><strong>7. Cancellation Policy</strong><br />\nCancellations within 72 - 48 hours of tour departure will incur a 50% loss of the ticket price. Cancellations within 48 hours of tour departure or failure to take up a tour will result in 100% loss of the ticket price.</p>\n\n<p><strong>8. iPods</strong><br />\nAudio language translations are available on some of our tours (Great Ocean Road day tours, Phillip Island day tours and Mornington Peninsula day tours). Our ipods are synced with Chinese, Japanese, Korean, French, German, Italian and Spanish tour information. These must be requested at time of booking.&nbsp; Please note however that although we will do our best to meet your request we are unable to guarantee availability.</p>\n\n<p><strong>9. Specific Dietary Requirements</strong><br />\nPlease advise us of any specific dietary requirements you may have at time of booking.&nbsp; Please note that although we will do our best to meet your request, we are unable to guarantee this will be met, as some of our tours include travel into remote areas and some specific requests can simply not be catered for e.g. vegan, gluten intolerance etc. we would suggest you bring some suitable snacks along for yourself to avoid any disappointment.</p>\n\n<p><strong>10. Internet &amp; Wi-fi</strong><br />\nWiFi is available on selected tours and vehicles.&nbsp; Please note we will do our best to make this available to you whenever and wherever possible, however it is not guaranteed.</p>\n\n<p>&nbsp;</p>",
            "status": "CONFIRMED",
            "confirmed": "2017-11-07T18:55:52.436+11:00",
            "created": "2017-11-07T18:55:53.054+11:00",
            "reservationItems": [{
                    "id": 5178,
                    "paxId": 8713,
                    "paxIndex": 0,
                    "productId": 900,
                    "productCid": 243,
                    "voucherId": "5178",
                    "idExternal": "-999999",
                    "retailRef": "GoQuo",
                    "productCode": "BGOR_B8S18T7",
                    "productName": "1D Great Ocean Road Tour Classic  - 1D Great Ocean Road Tour Classic",
                    "operatorRate": 130,
                    "retailRate": 130,
                    "retailRateOverride": 130,
                    "retailCommissionAmount": 0,
                    "retailCommissionPerc": 0,
                    "wholesaleCommissionAmount": 39,
                    "levies": [{
                        "amount": 0,
                        "description": "Levy Adult"
                    }],
                    "paxName": "Max, Magento",
                    "ticketInfo": "What to bring:\nSnacks, a water bottle and bathers for swimming. \n\nWhat to wear:\nAppropriate clothing for all weather conditions.\n\nConditions: \nDuring peak times Parks Vic may close Gibsons Beach from time to time due to safety concerns posed by excessive crowds. Also note that on Christmas Day the itinerary is slightly altered and you will not visit Cape Otway Lighthouse. You will also be provided with a picnic lunch on that day. \n(Main Departure Point)",
                    "paxNumber": 1,
                    "date": "2017-12-10",
                    "pickupId": 7996,
                    "pickupPointName": "Bunyip Tours Reception",
                    "pickupLatitude": -37.820765,
                    "pickupLongitude": 144.955432,
                    "pickupAddress": "570 Flinders St, Melbourne, VIC, 3000",
                    "pickupTime": "07:15:00.000",
                    "cancellationPolicy": [{
                            "hoursTillDeparture": 24,
                            "feePerc": 100
                        },
                        {
                            "hoursTillDeparture": 48,
                            "feePerc": 50
                        },
                        {
                            "hoursTillDeparture": 8760,
                            "feePerc": 25
                        }
                    ],
                    "created": "2017-11-07T18:55:53.051+11:00",
                    "ticket": "https://dev1.livngds.com/livngds/api/tickets/5178",
                    "wholesaleNetRate": 91
                },
                {
                    "id": 5179,
                    "paxId": 8714,
                    "paxIndex": 1,
                    "productId": 900,
                    "productCid": 243,
                    "voucherId": "5179",
                    "idExternal": "-999999",
                    "retailRef": "GoQuo",
                    "productCode": "BGOR_B8S18T7",
                    "productName": "1D Great Ocean Road Tour Classic  - 1D Great Ocean Road Tour Classic",
                    "operatorRate": 95,
                    "retailRate": 95,
                    "retailRateOverride": 95,
                    "retailCommissionAmount": 0,
                    "retailCommissionPerc": 0,
                    "wholesaleCommissionAmount": 28.5,
                    "levies": [{
                        "amount": 0,
                        "description": "Levy Child"
                    }],
                    "paxName": "Cen, Javis",
                    "ticketInfo": "What to bring:\nSnacks, a water bottle and bathers for swimming. \n\nWhat to wear:\nAppropriate clothing for all weather conditions.\n\nConditions: \nDuring peak times Parks Vic may close Gibsons Beach from time to time due to safety concerns posed by excessive crowds. Also note that on Christmas Day the itinerary is slightly altered and you will not visit Cape Otway Lighthouse. You will also be provided with a picnic lunch on that day. \n(Main Departure Point)",
                    "paxNumber": 2,
                    "date": "2017-12-10",
                    "pickupId": 7996,
                    "pickupPointName": "Bunyip Tours Reception",
                    "pickupLatitude": -37.820765,
                    "pickupLongitude": 144.955432,
                    "pickupAddress": "570 Flinders St, Melbourne, VIC, 3000",
                    "pickupTime": "07:15:00.000",
                    "cancellationPolicy": [{
                            "hoursTillDeparture": 24,
                            "feePerc": 100
                        },
                        {
                            "hoursTillDeparture": 48,
                            "feePerc": 50
                        },
                        {
                            "hoursTillDeparture": 8760,
                            "feePerc": 25
                        }
                    ],
                    "created": "2017-11-07T18:55:53.053+11:00",
                    "ticket": "https://dev1.livngds.com/livngds/api/tickets/5179",
                    "wholesaleNetRate": 66.5
                },
                {
                    "id": 5180,
                    "paxId": 8715,
                    "paxIndex": 2,
                    "productId": 900,
                    "productCid": 243,
                    "voucherId": "5180",
                    "idExternal": "-999999",
                    "retailRef": "GoQuo",
                    "productCode": "BGOR_B8S18T7",
                    "productName": "1D Great Ocean Road Tour Classic  - 1D Great Ocean Road Tour Classic",
                    "operatorRate": 95,
                    "retailRate": 95,
                    "retailRateOverride": 95,
                    "retailCommissionAmount": 0,
                    "retailCommissionPerc": 0,
                    "wholesaleCommissionAmount": 28.5,
                    "levies": [{
                        "amount": 0,
                        "description": "Levy Child"
                    }],
                    "paxName": "Thor, Javis",
                    "ticketInfo": "What to bring:\nSnacks, a water bottle and bathers for swimming. \n\nWhat to wear:\nAppropriate clothing for all weather conditions.\n\nConditions: \nDuring peak times Parks Vic may close Gibsons Beach from time to time due to safety concerns posed by excessive crowds. Also note that on Christmas Day the itinerary is slightly altered and you will not visit Cape Otway Lighthouse. You will also be provided with a picnic lunch on that day. \n(Main Departure Point)",
                    "paxNumber": 3,
                    "date": "2017-12-10",
                    "pickupId": 7996,
                    "pickupPointName": "Bunyip Tours Reception",
                    "pickupLatitude": -37.820765,
                    "pickupLongitude": 144.955432,
                    "pickupAddress": "570 Flinders St, Melbourne, VIC, 3000",
                    "pickupTime": "07:15:00.000",
                    "cancellationPolicy": [{
                            "hoursTillDeparture": 24,
                            "feePerc": 100
                        },
                        {
                            "hoursTillDeparture": 48,
                            "feePerc": 50
                        },
                        {
                            "hoursTillDeparture": 8760,
                            "feePerc": 25
                        }
                    ],
                    "created": "2017-11-07T18:55:53.054+11:00",
                    "ticket": "https://dev1.livngds.com/livngds/api/tickets/5180",
                    "wholesaleNetRate": 66.5
                }
            ],
            "type": "FIXED_DATE"
        }],
        "hasClaimedReservationItems": false,
        "created": "2017-11-07T18:55:42.174+11:00",
        "modified": "2017-11-07T18:55:53.278+11:00"
    }
}

// const result = transform(source, template)
// console.log(result)


const arr = [{
    '#text': 1
}, {
    '#text': 2
}]

const sightseeingPriceRequest = {
    Children:{
        Age: arr
    } 
}

const root = builder.create(sightseeingPriceRequest, {
    version: '1.0',
    encoding: 'UTF-8'
}).end()

console.log(root)