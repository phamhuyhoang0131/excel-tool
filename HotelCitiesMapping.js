const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://hotelgateway:SeechoitlOfwoRwObEaphOabdLVy@54.169.228.218:3306/HotelGateway'
})
const promise = require('bluebird')
const csv = require('fast-csv')
const fs = require('fs')
const _ = require('lodash')
const json2csv = require('json2csv')
const tranform = require('camaro')

const {
    map2Arr
} = require('./helper/string-similar')

function readCsv(path) {
    return new Promise((resolve, reject) => {
        let csv_arr = []
        fs.createReadStream(path)
            .pipe(csv())
            .on("data", function (data) {
                csv_arr.push(data)
            })
            .on("end", function () {
                resolve(csv_arr)
            });
    })

}
const progess = async() => {
    let listCities = await readCsv('./IndonesiaCityList.csv')
    listCities = _.flattenDeep(listCities)
    // remove header 
    listCities.shift()
    // find matched record in db
    const matchedCities = await promise.all(listCities.map(city => {
        return knex('city_mapping').whereRaw(`city_name like '%${city}%'`)
    }))
    listCities = listCities.map((city, idx) => {
        return {
            city,
            matched: _.flattenDeep(matchedCities[idx])
        }
    })
    // console.log(listCities)
    // fs.writeFileSync('indo.json', JSON.stringify(listCities))

    const mappedCities = listCities.filter(city => city.matched.length > 0)
    const notMappedCities = listCities.filter(city => city.matched.length === 0)
    // find in not mapped table 
    const citiesGTANotMapped = await promise.all(notMappedCities.map(c => {
        return knex('city_not_mapping').whereRaw(`provider_city_name like '%${c.city}%'`)
            .andWhere('provider_country_code', 'ID')
    }))

    const results = notMappedCities.map((c, idx) => {
        return {
            name: c.city,
            matchs: citiesGTANotMapped[idx]
        }
    })
    // fs.writeFileSync('results.json', JSON.stringify(results))
    // get final not match city 
    const notMatchResults = results.filter(r => r.matchs.length === 0)
    const matchResults = results.filter(r => r.matchs.length > 0)
    return matchResults
}
const insertMappedRecord = async(records) => {
    let arr = []
    records.map(r => {
        r.matchs.map(m => {
            let {
                id,
                provider_code,
                provider_city_code,
                provider_city_name,
                provider_country_code,
                country_code
            } = m

            arr.push({
                airport_code: '',
                city_name: r.name,
                provider_code: provider_code,
                provider_city_code: provider_city_code,
                provider_city_name: provider_city_name,
                provider_country_code: provider_country_code || country_code,
                note: 'Indonesia'
            })
        })

    })
    await knex.batchInsert('city_mapping', arr, 100)
    console.log("done insert db")
}
// progess().then(insertMappedRecord)


// find all mapping destination for all supplier 

const Adonis = async(listCities) => {
    const cities = await readCsv('./destinations/AdonisCities.csv')
    let convertCities = cities.map(city => {
        city = _.first(city)
        return (city) ? city.split('|') : city
    }).filter(c => c)
    // remove header csv file
    convertCities.shift()
    convertCities = convertCities.filter(city => {
        const [country_code, city_code, city_name] = city
        return (country_code == 221)
    })

    // find matching record
    const finalResult = map2Arr(listCities, _.uniq(convertCities.map(c => c[2])))
    return {
        finalResult,
        cities: convertCities
    }

}


const HotelBeds = async(listCities) => {
    // get mapping data for hotelbeds 
    let cities = await readCsv('./destinations/HotelBedsCities.csv')
    cities.shift()
    // filter destination at indonesia
    cities = cities.filter(city => _.first(city) == 'JP')
    // .map(city => {
    //     const [country_code, country_name, destination_code, destination_name] = city
    //     return destination_name
    // })
    // get unique city name
    // cities = (_.uniq(cities))
    // find matched record
    const finalResult = map2Arr(listCities, _.uniq(cities.map(city => city[3])))
    return {
        finalResult,
        cities
    }
}

const JacTravel = async(listCities) => {
    // get mapping data for Jactravel
    let cities = await readCsv('./destinations/JacTravelCites.csv')
    cities.shift()
    // filter destination at indonesia
    cities = cities.map(city => {
        city = _.first(city)
        return (city) ? city.split('|') : city
    }).filter(c => _.first(c) == 'Japan')
    // find matched record
    const finalResult = map2Arr(listCities, _.uniq(cities.map(c => c[2])))
    return {
        finalResult,
        cities
    }
}


const Miki = async(listCities) => {
    // get mapping data for Jactravel
    let cities = await readCsv('./destinations/MikiCities.csv')
    cities.shift()
    // filter destination at indonesia
    cities = cities.filter(city => _.first(city) == 'JP')
    //find matched record 
    const finalResult = map2Arr(listCities, _.uniq(cities.map(c => c[2])))
    return {
        finalResult,
        cities
    }
}

const Quantum = async(listCities) => {
    // get mapping data for Jactravel
    let cities = await readCsv('./destinations/QuantumCities.csv')
    cities.shift()
    // filter destination at indonesia
    cities = cities.filter(city => city[1] == 'JP')
    const finalResult = map2Arr(listCities, _.uniq(cities.map(c => c[2])))
    return {
        finalResult,
        cities
    }
}


const Travco = async(listCities) => {
    // get mapping data for Jactravel
    let cities = await readCsv('./destinations/TravcoCities.csv')
    cities.shift()
    // filter destination at Indonesia
    cities = cities.filter(city => city[1].trim() == 'JP').map(city => city.map(c => c.trim()))
    // get final result
    const finalResult = map2Arr(listCities, _.uniq(cities.map(city => city[5])))
    return {
        finalResult,
        cities
    }
}

const GTA = async(listCities) => {
    // get mapping data for Jactravel
    let cities = await readCsv('./destinations/GTACities.csv')
    cities.shift()
    // filter destination at Indonesia 
    cities = cities.filter(city => _.last(city) == 'JP')
    const finalResult = map2Arr(listCities, _.uniq(cities.map(city => city[1])))
    return {
        finalResult,
        cities
    }
}

const Travflex = async(listCities) => {
    // get mapping data for Travflex
    let cities = require('./destinations/TravflexCities.json')
    // filter destination at Indonesia
    cities = cities.filter(city => city.CountryName == 'Japan')
    const finalResult = map2Arr(listCities, _.uniq(cities.map(city => city.CityName)))
    return {
        finalResult,
        cities
    }
}

const DOTW = async(listCities) => {
    // get mapping data for DOTW
    let cities = await readCsv('./destinations/DOTWCities.csv')
    cities.shift()
    // filter destination at Indonesia
    cities = cities.filter(city => city[2] == 171)
    // 170 is indonesia code
    // get final result 
    const finalResult = map2Arr(listCities, _.uniq(cities.map(city => city[1])))
    return {
        finalResult,
        cities
    }
}

const FIT = async(listCities) => {
    // get mapping data for FIT
    let cities = await readCsv('./destinations/FITCities.csv')
    cities.shift()
    // filter destination at Indonesia
    cities = cities.filter(city => _.first(city) == 96)
    // get final resutl
    const finalResult = map2Arr(listCities, _.uniq(cities.map(city => city[6])))
    return {
        finalResult,
        cities
    }
}


const Agoda = async(listCities) => {

    // get mapping data for Agoda
    let rawData = fs.readFileSync('./destinations/AgodaCities.xml').toString()
    const template = {
        cities: ['//city', {
            city_id: 'city_id',
            country_id: 'country_id',
            city_translated: 'city_translated',
            city_name: 'city_name'
        }]
    }
    let {
        cities
    } = tranform(rawData, template)
    // filter destination at Indonesia
    cities = cities.filter(city => city.country_id == 192)
    // get final result 
    const finalResult = map2Arr(listCities, _.uniq(cities.map(city => city.city_name)))
    return {
        finalResult,
        cities
    }
}


const allMapping = {
    Adonis,
    HotelBeds,
    JacTravel,
    Miki,
    Quantum,
    Travco,
    GTA,
    Travflex,
    DOTW,
    FIT,
    Agoda
}

// Reverser mapping to find city id for mapped record 

/**
 * This function is provide ability for mapping a list city
 */
const ReserverMapping = async() => {
    // get list unmapped cities
    let listCities = await readCsv('./destinations/JapanCities.csv')
    listCities = _.flattenDeep(listCities)
    listCities.shift()
    // create empty array to hold all result from suppliers
    let results = []
    for (var key in allMapping) {
        if (allMapping.hasOwnProperty(key)) {
            let supplier = await allMapping[key](listCities)
            // filter record rating greater than 70% matched
            let {
                finalResult,
                cities
            } = supplier
            finalResult = finalResult.filter(f => f.rating >= 0.5)
            let parentRecord

            switch (key) {
                case 'Adonis':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c[2])
                        const [country_code, city_code, city_name] = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                country_code,
                                city_code,
                                city_name
                            }
                        }
                    })
                    results.push({
                        Adonis: parentRecord
                    })
                    break
                case 'HotelBeds':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c[3])
                        const [country_code, country_name, city_code, city_name] = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                country_code,
                                city_code,
                                city_name
                            }
                        }
                    })
                    results.push({
                        HotelBeds: parentRecord
                    })
                    break
                case 'JacTravel':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c[2])
                        const [country_name, city_code, city_name] = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                country_name,
                                city_code,
                                city_name
                            }
                        }
                    })
                    results.push({
                        JacTravel: parentRecord
                    })
                    break
                case 'Miki':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c[2])
                        const [country_code, city_code, city_name] = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                country_code,
                                city_code,
                                city_name
                            }
                        }
                    })
                    results.push({
                        Miki: parentRecord
                    })
                    break
                case 'Quantum':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c[2])
                        const [city_code, country_code, city_name] = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                country_code,
                                city_name,
                                city_code
                            }
                        }
                    })
                    results.push({
                        Quantum: parentRecord
                    })
                    break
                case 'Travco':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c[5])
                        const [country_id, country_code, country_name, city_id, city_code, city_name] = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                country_code,
                                country_name,
                                city_code,
                                city_name
                            }
                        }
                    })
                    results.push({
                        Travco: parentRecord
                    })
                    break
                case 'GTA':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c[1])
                        const [city_code, city_name, country_code] = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                city_code,
                                city_name,
                                country_code
                            }
                        }
                    })
                    results.push({
                        GTA: parentRecord
                    })
                    break
                case 'Travflex':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c.CityName)
                        const {
                            CityCode,
                            CityName,
                            CountryCode,
                            CountryName
                        } = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                city_code: CityCode,
                                city_name: CityName,
                                country_code: CountryCode,
                                country_name: CountryName
                            }
                        }
                    })
                    results.push({
                        Travflex: parentRecord
                    })
                    break
                case 'DOTW':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c[1])
                        const [city_code, city_name, country_code] = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                city_code,
                                city_name,
                                country_code
                            }
                        }
                    })
                    results.push({
                        DOTW: parentRecord
                    })
                    break
                case 'FIT':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c[6])
                        const [country_code, city_code, IATA1, IATA2, IATA3, IATA4, city_name] = match
                        return {
                            source_city_name : f.source,
                            matchedRecord: {
                                rating: f.rating,
                                city_code,
                                city_name,
                                country_code
                            }
                        }
                    })
                    results.push({
                        FIT: parentRecord
                    })
                    break
                case 'Agoda':
                    parentRecord = finalResult.map(f => {
                        const match = cities.find(c => f.target == c.city_name)
                        const {
                            city_id,
                            city_translated,
                            city_name,
                            country_id
                        } = match
                        return {
                            source_city_name: f.source,
                            matchedRecord: {
                                rating: f.rating,
                                city_code: city_id,
                                city_name: city_name,
                                country_code: country_id
                            }
                        }
                    })
                    results.push({
                        Agoda: parentRecord
                    })
                    fs.writeFileSync('results.json', JSON.stringify(results))
                default:
                    break
            }
        }
    }

}

// function export result from reserver mapping
const insertMapping = async(data) => {
    // get list unmapped cities
    let listCities = await readCsv('./destinations/JapanCities.csv')
    listCities = _.flattenDeep(listCities)
    listCities.shift()
    // get processed data
    const results = require('./results.json')
    let raw = []
    results.map(supplier => {
        for (let key in supplier) {
            if (supplier.hasOwnProperty(key)) {
                const supplierMapping = listCities.map(city => {
                    const matched = supplier[key].find(s => s.source_city_name == city)
                    if (matched) {
                        return {
                            source_city_name: matched.source_city_name,
                            supplier: key,
                            rating: matched.matchedRecord.rating,
                            city_code: matched.matchedRecord.city_code,
                            city_name: matched.matchedRecord.city_name,
                            country_code: matched.matchedRecord.country_code,
                            airport_code: ''
                        }
                    }
                    return undefined
                })
                raw.push(supplierMapping)
            }
        }
    })
    raw = _.flattenDeep(raw)
    raw = raw.filter(r => r)
    raw = _.groupBy(raw, 'source_city_name')
    let final = []
    let inMapping = []
    for (var key in raw) {
        if (raw.hasOwnProperty(key)) {
            // get city already mapping
            inMapping.push(key)
            var element = raw[key];
            final.push(element)
        }
    }
    // find city still not mapped
    let notMapped = listCities.filter(c => !inMapping.find(i => i == c))
    notMapped = notMapped.map(n => {
        return {
            source_city_name: n,
            supplier: '',
            rating: '',
            city_code: '',
            city_name: '',
            country_code: ''
        }
    })
    final = _.flattenDeep(final).concat(notMapped)
    final = json2csv({
        data: final,
        fields: ['source_city_name', 'supplier', 'rating', 'city_code', 'city_name', 'country_code', 'airport_code']
    })
    console.log(final)
    fs.writeFileSync('JapanMappingResult.csv', final)
}

ReserverMapping().then( () =>{
    insertMapping()
} )

// miki uniq case process


const insertMikiMapping = async() => {
    const datas = await readCsv('./static /mikimaping.csv')
    let airports = await knex('lst_airport')
    airports = airports.map(a => a.code)
    datas.shift()
    let toInsert = datas.map(record => {
        const [id, airport_code, city_name, provider_code,
            provider_city_code, provider_city_name,
            provider_country_code, note, del_flag,
            distance_km, similarity_rating, similarity_top_ratings
        ] = record
        return {
            airport_code,
            city_name,
            provider_code,
            provider_city_code,
            provider_city_name,
            provider_country_code,
            note,
            del_flag,
            similarity_rating,
            similarity_top_ratings
        }
    })
    // remove fucking record 
    toInsert = toInsert.filter(t => t.airport_code != 'KNDY')
    const totalAirport = _.uniq(airports.concat(toInsert.map(t => t.airport_code)))
    const diff = _.difference(totalAirport, airports)
    // console.log(diff)
    console.log(toInsert)
    // console.log(toInsert)
    await knex.batchInsert('city_mapping', toInsert, 1000).then(() => console.log('inserted'))
    console.log('success')
}

const insertMikiSubProduct = async() => {
    const datas = await require('./static /miki_sub_product_info.json')
    await knex.batchInsert('miki_sub_product_info', datas, 1000)
    console.log('success insert')
}

// insertMikiSubProduct().then()
// insertMikiMapping()


// New task progess  roomsXML Mapping 
const roomsXMLRegionMapping = async() => {
    let sources = await readCsv('./static /roomsXML-Region.csv')
    let countries = await readCsv('./static /roomsXML-NationalityCodes.csv')
    const cities = await knex.raw('SELECT distinct (city_name), airport_code FROM HotelGateway.city_mapping')
    // remove first line header 
    countries.shift()
    countries = countries.map(c => {
        const [country_name, country_iso_code] = c
        return {
            country_name,
            country_iso_code
        }
    })
    // cities = cities[0].map(c => c.city_name).filter(c => c)
    sources = sources.map(c => {
        const [region_id, region_name, state, country_id, country_name] = c
        return {
            region_id,
            region_name,
            state,
            country_id,
            country_name
        }
    })
    sources.shift()
    // get matching data 
    // console.time('timeMachine')
    // const matchingResult = map2Arr(sources.map(s => s.region_name) ,  cities)
    // fs.writeFileSync('matchingCityResult.json', JSON.stringify(matchingResult))
    // console.timeEnd('timeMachine')
    let results = require('./matchingCityResult.json')
    // console.log(results.filter(r => r.rating > 0.75).length)
    // just get record matching 100% with source city name
    const listAirports = _.first(cities)
    results = results.filter(r => r.rating == 1)
    results = results.map(r => {
        const {
            source,
            target,
            rating
        } = r
        const {
            region_id,
            region_name,
            state,
            country_id,
            country_name
        } = sources.find(s => s.region_name == source)
        // find iso country code 
        const countryMapped = map2Arr([country_name], countries.map(c => c.country_name)).find(m => m.rating == 1) || ''
        console.log(countryMapped)
        const matchedAirport = listAirports.find(l => l.city_name == target)
        return {
            source_dest_name: source,
            source_dest_code: region_id,
            destination_code: matchedAirport.airport_code,
            source_country_code: country_id,
            target,
            country_name: countryMapped ? countryMapped.country_name : '',
            country_iso_code: countryMapped ? countries.find(c => c.country_name).country_iso_code : ''
            // similarity_rating: 1,
            // note: 'roomsXML',
        }
    })
    fs.writeFileSync('finalx.json', JSON.stringify(results))
    console.log("số lượng map được : " + results.length)
    // get all airport mapping with one hotel replace 1-1 mapping 
    // const citiesByAirport = _.uniq(results.map(r => r.target))
    // let mappedRecords  = await knex('city_mapping').whereIn('provider_city_name', citiesByAirport)
    // mappedRecords = _.groupBy(mappedRecords, 'provider_city_name')
    // let arr = []
    // for (var key in mappedRecords) {
    //     if (mappedRecords.hasOwnProperty(key)) {
    //         var element = mappedRecords[key];
    //         arr.push({
    //             key,
    //             number: _.uniqBy(element, 'airport_code').length
    //         })
    //     }
    // }
    // console.log(arr.filter(a => a.number > 1))
    // fs.writeFileSync('sampleMapping.json', JSON.stringify(_.groupBy(mappedRecords, 'provider_city_name')) )

    // const csv = json2csv({
    //     data: results,
    //     fields: ['source_dest_name', 'source_dest_code', 'destination_code', 'source_country_code']
    // })
    // fs.writeFileSync('finalResultMappingRoomsXML.csv', csv)
}


const roomsXMLRegionMappingV2 = async() => {
    let sources = await readCsv('./static /roomsXML-Region-V2.csv')
    let countries = await readCsv('./static /roomsXML-NationalityCodes.csv')
    let cities = await knex('city_mapping').select('city_name', 'airport_code', 'provider_country_code')
    // remove first line header 
    countries.shift()
    sources.shift()
    countries = countries.map(c => {
        const [country_name, country_iso_code] = c
        return {
            country_name,
            country_iso_code
        }
    })
    sources = sources.map(c => {
        const [region_id, region_name, state, country_id, country_name, ISO] = c
        return {
            region_id,
            region_name,
            state,
            country_id,
            country_name,
            ISO
        }
    })
    const uniqCountry = _.uniq(sources.map(s => s.country_name))
    const uniqRawCountry = countries.map(c => c.country_name)
    const diff = _.difference(uniqCountry, uniqRawCountry)
    // get full country mapping 
    sources = sources.map(s => {
        // when not found iso code for city
        if (!s.ISO) {
            const country_mapping = countries.find(c => c.country_name == s.country_name)
            s.ISO = country_mapping.country_iso_code
        }
        return s
    })
    // mapping resource with table already mapping

    // const matchedRecords = []
    // sources.map(r => {
    //     // find iso code 
    //     const { ISO, country_name, region_id, region_name } = r
    //     const temp = cities.filter(c =>  {
    //         const { city_name, airport_code, provider_country_code} = c
    //         if(_.lowerCase(city_name).trim() == _.lowerCase(region_name).trim() && provider_country_code == ISO){
    //             return true
    //         }
    //     })
    //     if(temp && temp.length){
    //         matchedRecords.push({
    //             region_id,
    //             region_name,
    //             country_name,
    //             ISO,
    //             match: temp
    //         })
    //     }
    // })
    // fs.writeFileSync('matchedRecords.json', JSON.stringify(matchedRecords))
    // re mapping because one city can be find by mutilple airport 
    let results = require('./matchedRecords.json')
    results = results.filter(r => r.match.length)
    console.log(results.length)
    const finalResult = []
    results.map(r => {
        let {
            region_id,
            region_name,
            country_name,
            ISO,
            match
        } = r
        match = _.uniqBy(match, 'airport_code')

        match.map(m => {
            const {
                city_name,
                airport_code,
                provider_country_code
            } = m
            finalResult.push({
                source_dest_name: region_name,
                source_dest_code: region_id,
                destination_code: airport_code,
                source_country_code: ISO,
                supplier_code: 'RoomsXML'
            })
        })
    })

    const csv = json2csv({
        data: finalResult,
        fields: ['source_dest_name', 'source_dest_code', 'destination_code', 'source_country_code', 'supplier_code']
    })
    fs.writeFileSync('finalResultMappingRoomsXML.csv', csv)
}

const roomsXMLBruceForce = async() => {
    // get from 90 => 99% match 
    let cities = await knex('city_mapping').select('city_name', 'airport_code', 'provider_country_code')
    let sources = require('./matchingCityResult.json')
    let origin = await readCsv('./static /roomsXML-Region-V2.csv')
    let countries = await readCsv('./static /roomsXML-NationalityCodes.csv')
    countries.shift()
    origin.shift()
    countries = countries.map(c => {
        const [country_name, country_iso_code] = c
        return {
            country_name,
            country_iso_code
        }
    })
    origin = origin.map(o => {
        const [region_id, region_name, state, country_id, country_name, ISO] = o
        return {
            region_id,
            region_name,
            state,
            country_id,
            country_name,
            ISO
        }
    })
    sources = sources.filter(s => ((s.rating > 0.9) && (s.rating < 1)))
    origin = origin.map(o => {
        // when not found iso code for city
        if (!o.ISO) {
            const country_mapping = countries.find(c => c.country_name == o.country_name)
            o.ISO = country_mapping.country_iso_code
        }
        return o
    })
    sources = sources.map(s => {
        // find source ISO
        const {
            region_id,
            region_name,
            state,
            country_id,
            country_name,
            ISO
        } = origin.find(o => s.source == o.region_name)
        s.ISO = ISO
        s.region_id = region_id
        return s
    })
    // compress data to json
    // fs.writeFileSync('90percentMatch.json', JSON.stringify(sources))
    // progess data from manual matching file
    let manualData = require('./90percentMatch.json')
    manualData = manualData.filter(m => m.isMatch === true)
    manualData = manualData.map(m => {
        const {
            source,
            target,
            ISO,
            region_id
        } = m
        // find matched airport
        const {
            airport_code
        } = cities.find(c => c.city_name == target)
        return {
            source_dest_name: source,
            source_dest_code: region_id,
            destination_code: airport_code,
            supplier_code: 'RoomsXML'

        }
    })
    const csv = json2csv({
        data: manualData,
        fields: ['source_dest_name', 'source_dest_code', 'destination_code', 'source_country_code', 'supplier_code']
    })
    fs.writeFileSync('ManualMappingRoomsXML.csv', csv)
}

const compareRoomsXMLMapping = async() => {
    let sourceTest = await readCsv('./static /roomsXML-Region-V2.csv')
    let sourceLive = await readCsv('./static /roomsXML-Region-Live.csv')
    sourceTest = sourceTest.map(source => {
        const [region_id, region_name, state, country_id, country_name] = source
        return {
            region_id,
            region_name,
            state,
            country_id,
            country_name
        }
    })
    sourceLive = sourceLive.map(src => {
        const [region_id, region_name, state, country_id, country_name] = src
        return {
            region_id,
            region_name,
            state,
            country_id,
            country_name
        }
    })
    const results = _.differenceWith(sourceLive, sourceTest, _.isEqual)
    console.log(results)
    // mapped 2 missing mapping 3 => done

}

const roomsXMLMappingV3 = async() => {
    const roomsXMLData = await readCsv('./static /roomsXMLMappingReport.csv')
    roomsXMLData.shift()

    let countries = await readCsv('./static /roomsXML-NationalityCodes.csv')
    countries.shift()
    let cities = await knex('city_mapping').select('city_name', 'airport_code', 'provider_country_code')
    const rawData = []
    countries = countries.map(c => {
        const [country_name, code] = c
        return {
            country_name,
            code
        }
    })
    console.log("số lượng data tổng => " + roomsXMLData.length)
    roomsXMLData.map(r => {
        const [region_id, region_name, state, country_id, country_name, airport_code] = r
        if (airport_code != "" && _.lowerCase(airport_code) != 'n') {
            rawData.push({
                region_id,
                region_name,
                state,
                country_id,
                country_name,
                airport_code,
                country_code: ''
            })
        }
    })
    // final mapping result
    let finalMappingResult = []
    const v2 = []
    const v3 = []
    rawData.map(r => {
        const countryMapped = countries.find(c => c.country_name == r.country_name)
        if (countryMapped) {
            r.country_code = countryMapped.code
            // convert to v2 
            v2.push({
                supplier: 'RoomsXML',
                similarity_rating: 1,
                city_name: r.region_name,
                provider_city_code: r.region_id,
                provider_city_name: r.region_name,
                provider_country_code: r.country_code,
                note: 'New RoomsXML Mapping',
                airport_code: r.airport_code
            })
            // convert to v3
            v3.push({
                supplier_code: 'RoomsXML',
                destination_code: r.airport_code,
                source_dest_code: r.region_id,
                source_dest_name: r.region_name,
                source_country_code: r.country_code
            })
            finalMappingResult.push(r)
        }
    })
    console.log(finalMappingResult)
    console.log("số lượng cuối cùng của roomXML => " + finalMappingResult.length)
    // output to file
    const v2CSV = json2csv({
        data: v2,
        fields: [ 'supplier', 'similarity_rating', 'city_name','provider_city_code', 'provider_city_name', 'provider_country_code', 'note', 'airport_code']
    })
    const v3CSV = json2csv({
        data: v3,
        fields: ['supplier_code', 'destination_code', 'source_dest_code', 'source_dest_name', 'source_country_code']
    })
    fs.writeFileSync('RoomsXMLV2Mapping.csv', v2CSV )
    fs.writeFileSync('RoomsXMLV3Mapping.csv', v3CSV)
}

// compareRoomsXMLMapping()
// roomsXMLBruceForce()


// roomsXMLRegionMappingV2()

// roomsXMLRegionMapping()


// roomsXMLMappingV3().then()


const indocititesV2 = async() => {
    const data = await readCsv('./static /IndonesiaCityListVer2.csv');
    const headers = data.shift()
    const dataFilter = []
    data.map(d => {
        const [cityNameUpperKey, cityNameLowerKey, match, isExist, airportCode, condition] = d
        if (isExist == 'New' && condition != 'not found') {
            dataFilter.push({
                cityNameUpperKey,
                cityNameLowerKey,
                airportCode,
                condition
            })
        }
    })
    // console.log(dataFilter)
    // revert mapping with city and airport
    const listAllCities = []
    let cities = await readCsv('./IndonesiaMappingResult.csv');
    const _headers = cities.shift()
    cities = cities.map(c => {
        const [source_city_name, supplier, rating, city_code, city_name, country_code] = c
        return {
            source_city_name,
            supplier,
            similarity_rating: rating,
            city_name: source_city_name,
            provider_city_code: city_code,
            provider_city_name: city_name,
            provider_country_code: country_code,
            note: 'New Indonesia City',
            airport_code: '',
            remark: ''
        }
    })
    const finalMappingResultv2 = []
    const finalMappingResultv3 = []
    cities.map(c => {
        const airport = dataFilter.find(d => d.cityNameUpperKey == c.source_city_name)
        if (airport) {
            // assaign airport code for city 
            c.airport_code = airport.airportCode
            c.remark = airport.condition

            finalMappingResultv2.push(c)
            // translate obj => v3
            finalMappingResultv3.push({
                supplier_code: c.supplier,
                destination_code: c.airport_code,
                source_dest_code: c.provider_city_code,
                source_dest_name: c.city_name,
                source_country_code: c.provider_country_code,
                remark: airport.condition
            })
        }
    })
    // export result for v2
    const v2 = json2csv({
        data: finalMappingResultv2,
        fields: ['source_city_name', 'supplier', 'similarity_rating', 'city_name',
            'provider_city_code', 'provider_city_name', 'provider_country_code', 'note', 'airport_code', 'remark'
        ]
    })
    fs.writeFileSync('IndonesiaNewCitiesV2.csv', v2)
    // translate to v3

    const v3 = json2csv({
        data: finalMappingResultv3,
        fields: ['supplier_code', 'destination_code', 'source_dest_code', 'source_dest_name', 'source_country_code', 'remark']
    })
    fs.writeFileSync('IndonesiaNewCitiesV3.csv', v3)
    console.log("done")
}

// indocititesV2().then()