let csv = require('fast-csv')
let fs = require('fs')
let _ = require('lodash')
function readCsv(path) {
   return new Promise( (resolve, reject) => {
        let csv_arr = []
        fs.createReadStream(path)
            .pipe(csv())
            .on("data", function (data) {
                csv_arr.push(data)
            })
            .on("end", function () {
                resolve(csv_arr)
            });
    })

}

class dataObj {
    constructor(name, idx) {
        this.name = name
        this.idx = idx
    }
}

function splitByParent(parentArr, childArr) {
    let slice_arr = []
    for (var i = 0; i < parentArr.length; i++) {
        let start = parentArr[i].idx,
            end = parentArr[i + 1] ? parentArr[i + 1].idx : childArr.length
        childArr.slice(start, end).map(obj => {
            obj = parentArr[i].name
            slice_arr.push(obj)
            return obj
        })
    }
    return slice_arr
}


function findMyWay(finalData, local, destination){
    let routes = []
    // find destination 
    let departure_ids = [] 
    finalData[0].map((obj, idx) => {
        if (obj === local) {
            departure_ids.push(idx)
        }
    })
    finalData.map((obj, index) => {
        if (index > 0) {
            // routes = routes.concat(generateRoutes(local, destination, departure_ids, index, finalData))
            // search
            switch (index) {
                case 1:
                    departure_ids.map(idx => {
                        if(finalData[index][idx] === destination){
                            routes.push(`${local}-${finalData[index][idx]}`)
                        }
                    })
                    break
                case 2:
                    departure_ids.map(idx =>{
                        if(finalData[index][idx] === destination){
                            routes.push(`${local}-${finalData[index -1][idx]}-${finalData[index][idx]}`)
                        }
                    })
                    break
                case 3:
                    departure_ids.map(idx =>{
                        if(finalData[index][idx] === destination){
                            routes.push(`${local}-${finalData[index -2][idx]}-${finalData[index -1][idx]}-${finalData[index][idx]}`)
                        }
                    })
                    break
                case 4:
                    departure_ids.map(idx => {
                        if (finalData[index][idx] === destination) {
                            routes.push(`${local}-${finalData[index - 3][idx]}-${finalData[index - 2][idx]}-${finalData[index - 1][idx]}-${finalData[index][idx]}`)
                        }
                    })
                    break
            }

        }
    })

    // filter results
    routes = _.uniq(routes)
    return routes
}
readCsv('./GOQUO-sales-routes-1.csv').then(data => {
    // remove two line header 
    data = data.slice(2, data.length)
    let allLevel = {
        lv0_all:[],
        lv0: [],
        lv1_all: [],
        lv1: [],
        lv2_all: [],
        lv2: [],
        lv3_all: [],
        lv3: [],
        lv4_all: [],
        lv4: []
    }
    data.map((obj, idx) => {
        // get data level 0
        allLevel.lv0_all.push(obj[1])
        if (obj[1]) {
            allLevel.lv0.push(new dataObj(obj[1], idx))
        }
        // get data lv 1
        allLevel.lv1_all.push(obj[3])
        if (obj[3]) {
            allLevel.lv1.push(new dataObj(obj[3], idx))
        }
        // get data lv2
        allLevel.lv2_all.push(obj[5])
        if(obj[5]){
            allLevel.lv2.push(new dataObj(obj[5], idx))
        }
        // get data lv 3

        allLevel.lv3_all.push(obj[7])
        if(obj[7]){
            allLevel.lv3.push(new dataObj(obj[7], idx))
        }
        // get data lv4
        allLevel.lv4_all.push(obj[9])
        if(obj[9]){
            allLevel.lv4.push(new dataObj(obj[9], idx))
        }
    })
    let finalData =[]
    finalData.push(splitByParent(allLevel.lv0, allLevel.lv0_all))
    finalData.push(splitByParent(allLevel.lv1, allLevel.lv1_all))
    finalData.push(splitByParent(allLevel.lv2, allLevel.lv2_all))
    finalData.push(splitByParent(allLevel.lv3, allLevel.lv3_all))
    finalData.push(splitByParent(allLevel.lv4, allLevel.lv4_all))
    let routes = findMyWay(finalData, 'HKG', 'KIX')
    console.log(routes)
    console.log(detectWrongRouter(routes))
})

function detectWrongRouter(routes) {
    let backup_routes = routes.filter(s => s.split('-').length <= 2)
    let filter_routes = routes.filter(s => s.split('-').length > 2)
    filter_routes = filter_routes.filter((obj, idx) => {
        let routesize = obj.split('-'),
            flag = true
        for (var i = 0; i < routesize.length; i++) {
            if (routesize[i + 2] === routesize[i]) {
                flag = false
            }
        }
        return flag
    })
    // merge router 
    return filter_routes.concat(backup_routes)
}
