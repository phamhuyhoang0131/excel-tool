let camaro = require('camaro')
let fs = require('fs')
let _ = require('lodash')
let bluebird = require('bluebird')
let builder = require('xmlbuilder')
const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://hotelgateway:SeechoitlOfwoRwObEaphOabdLVy@54.169.228.218:3306/HotelGatewayNew'
})

let request = require("request-promise")

// get all  HotelIds
let idx = 0
async function getAllHotelIds(paginateToken = ''){


    const parameters = {
        accessToken:
        'jnNUc4f98TgHZUUhc9egcRsHakv4UIBLua/fOGsdHbEfy8dxUVkTOecWu/VGeVQ+',
        client: '',
        test: true,
        showPackageRates: true,
        bookingEmail: 'ajanthan@goquo.com'
    }

    const params = Object.keys(parameters).map(key => ({
        '@key': key,
        '@value': parameters[key]
    }))

    let hotelListReq = {
        HotelListRQ: {
            timeoutMilliseconds: 100000,
            source: {
                languageCode: 'en'
            },
            filterAuditData: {
                registerTransactions: true
            },
            ContinuationToken:{
                '@expectedRange': 50000,
                '#text':paginateToken
            },
            Configuration: {
                User: '',
                Password: '',
                UrlGeneric: 'http://tthotel.xmltravelgate.com',
                Parameters: {
                    Parameter: params
                }
            }
        }
    }
    const soapRequest = {
        'soapenv:Envelope': {
            '@xmlns:soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
            '@xmlns:ns': 'http://schemas.xmltravelgate.com/hub/2012/06',
            '@xmlns:wsse':
            'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
            'soapenv:Header': {
                'wsse:Security': {
                    'wsse:UsernameToken': {
                        'wsse:Username': 'goquo',
                        'wsse:Password':'9V8vUYsq1b'
                    }
                }
            },
            'soapenv:Body': {
                'ns:HotelList': {
                    'ns:hotelListRQ': {
                        'ns:timeoutMilliseconds': 100000,
                        'ns:version': 1,
                            'ns:providerRQ': [
                                {
                                    'ns:code': 'TTHOTTEST',
                                    'ns:id': 1,
                                    'ns:rqXML': hotelListReq
                                }
                            ]
                    }
                }
            }
        }
    }

    let xml = builder.create(soapRequest, { encoding: 'utf-8' }).end()
    console.log(xml)
    var options = {
        method: 'POST',
        uri: 'http://hubhotelbatch.xmltravelgate.com/Service/Travel/v2/HotelBatch.svc',
        headers: {
            'content-type': 'text/xml; charset=utf-8',
            'soapaction': 'http://schemas.xmltravelgate.com/hub/2012/06/IServiceHotelBatch/HotelList'
        },
        body: xml
    }
    let data = await request(options)
        // parse xml repsonse
    data = _.unescape(data)
    let listHotels = await camaro(data, {
        paginateToken: '//ContinuationToken',
        hotels: ['//Hotel', {
            id: 'Code',
            name: 'Name',
            address: 'Address',
            town: 'Town',
            zip_code: 'ZipCode',
            country_code: 'CountryISOCode',
            destination_code: 'GeographicDestination/@code',
            star: 'CategoryCode',
            destination_name: 'GeographicDestination/@name',
            longitude: 'Longitude',
            latitude: 'Latitude',
            airport_code: '#',
            giata_id: 'GiataId',
            giata_url: 'GiataId/@source',
            giata_value: 'GiataId/@value'

        }]
    })
    paginateToken = listHotels.paginateToken
    const {hotels} = listHotels
    const giataData = hotels.find(h=> h.giata_id)
    if(giataData){
        console.log("giata code detected => "+giataData.giata_id)
    }
    if(paginateToken){
        idx++
        console.log("Index step : "+idx)
        console.log("paginate token: "+paginateToken)
        getAllHotelIds(paginateToken)
    }
    if(idx > 1 && paginateToken.length < 10 ) {
        // when nothing more to get
        console.log("done!")
        return null
    }

}

getAllHotelIds()


// // Step 1: get raw data from xml
// function getRawDestinationData() {
//     const xml = fs.readFileSync('./destination_list.xml', 'utf-8')
//     const template = {
//         data: ['//DestinationTree', {
//             code: "@code",
//             name: "@name",
//             child: ["DestinationLeaf", {
//                 child_code: '@code'
//             }]
//         }]
//     }

//     const result = camaro(xml, template)
//     // write file
//     fs.writeFileSync(`${__dirname}/XmlGateListDestination.json`, JSON.stringify(result), err => {
//         if (err) {
//             console.log("write file false" + err)
//         }
//         console.log("success")
//     })

// }



// class Destination {
//     constructor(parent_code, parent_name, children_code, children_name){
//         this.parent_code = parent_code
//         this.parent_name = parent_name
//         this.children_code = children_code
//         this.children_name = children_name
//     }
// }

// // Step 2: mapping parent destination with child destination
// // category data
// function destinationMapping() {
//     let listDestination = require(`${__dirname}/XmlGateListDestination.json`)

//     let parent = [],
//         children = [],
//         parentChildren = []

//     listDestination.data.map(obj => {
//         if (obj.child.length > 0 && obj.code.length == 2) {
//             parent.push(obj)
//         } else {
//             children.push(obj)
//         }
//     })

//     parent.map(p => {
//         let childIds = p.child.map(c => c.child_code)
//         let mappingChildren = children.filter(child => childIds.includes(child.code))

//         mappingChildren.map(obj => {
//             if (obj.name.includes("Resto")) {
//                 // fine restore data by code
//                 let restoreObj = children.find(c => c.code === obj.code)
//                 restoreObj.child.map(c => {
//                     // find correct mapped chilren with restore data 
//                     let correctChildren = children.find(child => child.code === c.child_code)
//                     parentChildren.push(new Destination(p.code, p.name, correctChildren.code, correctChildren.name))
//                 })
//             } else {
//                 parentChildren.push(new Destination(p.code, p.name, obj.code, obj.name))

//             }
//         })

//     })

//     fs.writeFileSync(`${__dirname}/XmlGateMappingDestination.json`, JSON.stringify(parentChildren))
// }


// // step 3: mapping air_port with supplier data
// let destinationData = require(`${__dirname}/XmlGateMappingDestination.json`)
// let mappedData = []
// async function mappingStep() {
//     let data = await knex("lst_airport")
//     let airportMapping = []
//     data.map(airport => {
//         let match = destinationData.find(des => (des.parent_code === airport.country_code && 
//         des.children_name.toLowerCase() == airport.city_name.toLowerCase() ))
//        if(match){
//            // add more mapping information
//            match.airport_id = airport.id
//            match.airport_code  = airport.code
//            match.airport_name = airport.name
//            mappedData.push(match)
//        }
//     })
//     return fs.writeFileSync(`${__dirname}/XmlGateAirportMappingDestination.json`, JSON.stringify(mappedData))
// }
// // mappingStep()
// // console.log("done")
// // step 4: get hotel id

// let finalMappingData = []

// function finalMapping() {
//     let destination = require('./XmlGateAirportMappingDestination.json')

//     for (let x = 0; x < 11; x++) {
//         console.log("start progess part - "+ x)
//         let hotels = fs.readFileSync(`./ListHotelXmlGate${x}.xml`, 'utf-8')
//         hotels = _.unescape(hotels)
//         let hotelTemplate = {
//             data: ['//Hotel', {
//                 hotel_id: 'Code',
//                 hotel_name: 'Name',
//                 country_code: 'CountryISOCode',
//                 destination_code: 'GeographicDestination/@code'

//             }]
//         }
//         let listHotels = camaro(hotels, hotelTemplate).data
//         mappingHotelDestination(destination, listHotels)
//         forceGC()
//         console.log("done part - "+ x)
//     }
//     finalMappingData = _.flatten(finalMappingData)
//     fs.writeFileSync(`${__dirname}/XmlGateHotelMappingDestination.json`,JSON.stringify(finalMappingData))
//     console.log(`success mapping - total : ${finalMappingData.length} items`)
// }

// // finalMapping()

// function mappingHotelDestination(mappedDestination, listHotels){
//     mappedDestination.map(des => {
//         // map destination and hotel by destination_code - country_code
//         let hotelMapping = listHotels.
//         filter(hotel => (des.parent_code == hotel.country_code && des.children_code == hotel.destination_code))
//         .map(hotel =>{
//             hotel.airport_code = des.airport_code
//             hotel.airport_name = des.airport_name
//             hotel.airport_id = des.airport_id
//             return hotel
//         })
//         finalMappingData.push(hotelMapping)
//     })
//     // call grabage collector
//     forceGC()
// }


// function forceGC() {
//     if (global.gc) {
//         global.gc();
//     } else {
//         console.warn('No GC hook! Start your program as `node --expose-gc file.js`.');
//     }

// }

// // get all hotel include mapped or not
// function getAllHotel() {
//     let hotelMapped = require('./XmlGateHotelMappingDestination.json')
//     let allHotel = []
//     for (let x = 0; x < 11; x++) {
//         console.log("start progess part - " + x)
//         let hotels = fs.readFileSync(`./ListHotelXmlGate${x}.xml`, 'utf-8')
//         hotels = _.unescape(hotels)
//         let hotelTemplate = {
//             data: ['//Hotel', {
//                 id: 'Code',
//                 name: 'Name',
//                 address: 'Address',
//                 town: 'Town',
//                 zip_code: 'ZipCode',
//                 country_code: 'CountryISOCode',
//                 destination_code: 'GeographicDestination/@code',
//                 star: 'CategoryCode',
//                 destination_name: 'GeographicDestination/@name',
//                 longitude: 'Longitude',
//                 latitude: 'Latitude',
//                 airport_code: '#'
//             }]
//         }

//         allHotel.push(camaro(hotels, hotelTemplate).data)
//         forceGC()
//         console.log("done part - " + x)
//     }
//     console.time("mapping")
//     allHotel = _.flatten(allHotel).map((hotel, idx) => {
//         let mappedRecord = hotelMapped.find(h => h.hotel_id === hotel.id)
//         if (idx % 10000 === 0) {
//             console.log("clear memory : "+idx)
//             forceGC()
//         }
//         if(mappedRecord){
//             hotel.airport_code = mappedRecord.airport_code
//         }
//         return hotel
//     })
//     console.timeEnd("mapping")
//     forceGC()
//     fs.writeFileSync(`${__dirname}/XmlGateHotelList.json`, JSON.stringify(allHotel))
//     console.log("done")
// }

// // getAllHotel().then(insertDB)
// // Step 5: Insert all hotel list to database
// async function insertDB() {
//     let data = require('./XmlGateHotelList.json')
//     await knex.batchInsert('lst_hotel_travelgate', data, 1000)
//     console.log('insert complete')
// }

// // insertDB()
