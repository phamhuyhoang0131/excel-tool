'use strict'

let stringSimilarity = require('string-similarity'),
    Lazy = require('lazy.js')

function map2Arr(sourceArr, compareArr) {

    let bestMatches = []

    sourceArr.map(str => {

        let matches = stringSimilarity.findBestMatch(str, compareArr),
            toReturn = {
                source: str,
                target: matches.bestMatch.target,
                rating: matches.bestMatch.rating
            }

        if (matches.bestMatch.rating < 1) {
            toReturn.topRatings = topRatings(matches.ratings, 10)
        }

        bestMatches.push(toReturn)
    })

    return bestMatches
}


function bestMatch(str, arr) {
    return stringSimilarity.findBestMatch(str, arr).bestMatch
}

function topRatings(ratings, topRecords) {
    return Lazy(ratings).sortBy(function(compare) {
        return compare.rating
    }, true).first(topRecords).value()
}

module.exports = {
    map2Arr: map2Arr,
    bestMatch: bestMatch,
    topRatings: topRatings
}
