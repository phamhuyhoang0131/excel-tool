let camaro = require('camaro'),
    fs = require('fs'),
    _ = require('lodash'),
    bluebird = require('bluebird'),
    builder = require('xmlbuilder'),
    geolib = require('geolib')
const knex = require('knex')({
    client: 'mysql2',
    connection: 'mysql://hotelgateway:SeechoitlOfwoRwObEaphOabdLVy@54.169.228.218:3306/HotelGatewayNew'
})




async function mappingHotel(){
    let listDestination = await knex('lst_destination')
   
    let allHotel = []
    for (let x = 0; x < 11; x++) {
        console.log("start progess part - " + x)
        let hotels = fs.readFileSync(`./ListHotelXmlGate${x}.xml`, 'utf-8')
        hotels = _.unescape(hotels)
        let hotelTemplate = {
            data: ['//Hotel', {
                id: 'Code',
                name: 'Name',
                address: 'Address',
                town: 'Town',
                zip_code: 'ZipCode',
                country_code: 'CountryISOCode',
                destination_code: 'GeographicDestination/@code',
                star: 'CategoryCode',
                destination_name: 'GeographicDestination/@name',
                longitude: 'Longitude',
                latitude: 'Latitude',
                airport_code: '#'
            }]
        }

        allHotel.push(camaro(hotels, hotelTemplate).data)
        forceGC()
        console.log("done part - " + x)
    }
    allHotel = _.flatten(allHotel).map(hotel => {
        let listAirportByCountry = listDestination.filter(des => (des.country_code === hotel.country_code) && (des.longitude && des.latitude))
        if (listAirportByCountry && listAirportByCountry.length > 0  && hotel.latitude && hotel.longitude ) {
            let hotelLocation = {
                latitude: hotel.latitude,
                longitude: hotel.longitude
            }
            listAirportByCountry  = formatGeoPoint(listAirportByCountry)
            listAirportByCountry[hotel.id] = hotelLocation
            let matchedAirport = geolib.findNearest(listAirportByCountry[hotel.id], listAirportByCountry, 1)
            hotel.airport_code = matchedAirport.key
            console.log(matchedAirport)
            console.log(geolib.convertUnit('km', matchedAirport.distance, 2))
        }
        return hotel
    })
         fs.writeFileSync(`${__dirname}/XmlGateHotelList.json`, JSON.stringify(allHotel))
         console.log('success mapping')
}






async function mappingHotelV2(x, listDestination) {
    let mappedHotel = []
    let allHotel = []
    console.log("mapping idx:" + x)
    console.time("progess time")
    let hotels = await fs.readFileSync(`./ListHotelXmlGate${x}.xml`, 'utf-8')
    hotels = _.unescape(hotels)
    let hotelTemplate = {
        data: ['//Hotel', {
            id: 'Code',
            name: 'Name',
            address: 'Address',
            town: 'Town',
            zip_code: 'ZipCode',
            country_code: 'CountryISOCode',
            destination_code: 'GeographicDestination/@code',
            star: 'CategoryCode',
            destination_name: 'GeographicDestination/@name',
            longitude: 'Longitude',
            latitude: 'Latitude',
            airport_code: '#'
        }]
    }

    allHotel.push(camaro(hotels, hotelTemplate).data)
    forceGC()
    allHotel = _.flatten(allHotel)
    listDestination.map(des => {
       
        let hotelMappedCountry = allHotel.filter(hotel => (hotel.country_code == des.country_code) && hotel.latitude && hotel.longitude)
        if (hotelMappedCountry && hotelMappedCountry.length > 0 && des.latitude && des.longitude) {
            hotelMappedCountry.map(hotel => {
                if (getDistanceBetweenXY(des.latitude, des.longitude, hotel.latitude, hotel.longitude) <= 50) {
                    hotel.airport_code = des.code
                    mappedHotel.push(hotel)
                }
            })
        }
        forceGC()
    })
    await fs.writeFileSync(`${__dirname}/TestHotelList${x}.json`, JSON.stringify(mappedHotel))


    console.timeEnd("progess time")
    console.log('success mapping')
    return true
}


function forceGC() {
    if (global.gc) {
        global.gc();
    } else {
        console.warn('No GC hook! Start your program as `node --expose-gc file.js`.');
    }

}


let getDistanceBetweenXY = (x, y, x1, y1) => {
    if(!x1 || !y1){
        return 10000 // return false value
    }
    let distance = geolib.getDistance({latitude: x, longitude: y},{latitude: x1, longitude: y1})

    return geolib.convertUnit('km', distance, 2)
}

let formatGeoPoint = (data) => {
    let geoObj = {}
    data.map(obj => {
        geoObj[obj.code] = {
            latitude: obj.latitude,
            longitude: obj.longitude
        }
    })
    return geoObj
}

// var spots = {
//     "Brandenburg Gate, Berlin": {latitude: 52.516272, longitude: 13.377722},
//     "Dortmund U-Tower": {latitude: 51.515, longitude: 7.453619},
//     "London Eye": {latitude: 51.503333, longitude: -0.119722},
//     "Kremlin, Moscow": {latitude: 55.751667, longitude: 37.617778},
//     "Eiffel Tower, Paris": {latitude: 48.8583, longitude: 2.2945},
//     "Riksdag building, Stockholm": {latitude: 59.3275, longitude: 18.0675},
//     "Royal Palace, Oslo": {latitude: 59.916911, longitude: 10.727567}
// }

// // in this case set offset to 1 otherwise the nearest point will always be your reference point
// console.log(geolib.findNearest(spots['Dortmund U-Tower'], spots, 1))

async function multilThreadMapping() {
    let listDestination = await knex('lst_destination')
    let promises = []

    for (x = 7; x < 11; x++) {
        console.log(x)
       promises.push(mappingHotelV2(x, listDestination))
    }

    return bluebird.all(promises)
}

// multilThreadMapping().then(console.log)


async function insertDB() {
    let hotels = []
    for(var x=0; x< 11; x++){
        hotels.push(require(`./TestHotelList${x}.json`))
    }
    hotels = _.flatten(hotels)
    await knex.batchInsert('lst_hotel_travelgate', hotels, 1000)
    console.log('insert complete')
}
insertDB()